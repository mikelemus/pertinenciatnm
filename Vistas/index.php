<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">			
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/Footer.css">
	
	<!-- <link rel="stylesheet" href="css/bootstrap-them.min.css"> -->
	<title>Programa de Pertinencia</title>
</head>
<body>
	<div class="container">
		<header class="row">
			<div class="col-md-12">
				<img src="imagen/HeaderCIITNE.jpg" class="img-responsive" alt="Imagen responsive" height="160" width="1140"	>
				<?php require 'nav.php'; ?>
			</div>
		</header>	
		<div class="col-md-12">
			<section class="main row">
				<article class="col-md-12">
					<h2>Tec vallarta</h2>

					<p></p><hr>
				</article>
			</section>
			<section class="k row">
				<div class="col-md-4">
					<h3>Programa para Empleadores</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
				<div class="col-md-4">
					<h3>Programa para  Egresados</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
				<div class="col-md-4">
					<h3>Beneficios  </h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</section>
			<section class="map row">
				<div class="col-md-12">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14933.454566727512!2d-105.1979076!3d20.6547796!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xaa7d5e1442b4b70e!2sTec+Vallarta%2C+Universidad+en+Puerto+Vallarta!5e0!3m2!1ses-419!2smx!4v1467584301730" style="border:0" allowfullscreen="" height="350" frameborder="0" width="100%"></iframe><br><br>
				</div>
			</section>

			<footer class="row">
				<div class="col-md-12">
			        <p>Footer 
			          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam error maxime quasi delectus doloremque, incidunt ratione ea ipsa expedita. Rerum nesciunt consectetur corporis suscipit error dolor, sint quas recusandae minus!
			        </p>
				</div>
			</footer>
		</div>
	</div>
</body>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
</html>