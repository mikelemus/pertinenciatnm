<!DOCTYPE html>
<html lang="en">
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid"> <!-- container-fluid-->
        <div class="navbar-header">
            <a href="#" class="navbar-brand"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Menu</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://189.254.201.245:8080/pertinenciatnm/Vistas/index.php">Programa de Pertinencia</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="item itemNav"><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/index.php" ><span class="glyphicon glyphicon-home"> </span> Inicio</a></li>
              
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Empleadores <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Empleadores.php">Cuestionario para Empleadores</a></li>
                </ul>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Egresados <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/itics.php">ITIC`s</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/isc.php">Ing. en Sistemas Computacionales</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/Electro.php">Ing. en Electromecanica</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/gestion.php">Ing. Gestion Empresarial</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/turismo.php">Lic. Turismo</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/arqui.php">Arquitectura</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/gastro.php">Gastronomia</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/animacion-digital.php">Ing.en Animación Digital Y Efectos Visuales</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/ContadorP.php">Contador Publico</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/ing-administracion.php">Ing. en Administración</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/ing-ambiental.php">Ing. en Ambiental</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/ing-civil.php">Ing. Civil</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/ing-electronica.php">Ing. Electronica</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/ing-energias-renovables.php">Ing. en Energias renovables</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/alimentarias.php">Ing. en Industrias Alimentarias</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/agricola.php">Ing. en Innovación Agrícola Sustentable</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/automotrices.php">Ing. en Sistemas Automotrices</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/mecatronica.php">Ing. en Mecatronica</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/informatica.php">Ing. Informatica</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/industrial.php">Ing. Industrial</a></li>
                    <li><a href="http://189.254.201.245:8080/pertinenciatnm/Vistas/Egresados/licAdmin.php">Lic. en Administración</a></li>   
                </ul>
              </li>
            </ul>
        </div><!-- navbar-collapse collapse -->
    </div><!-- container-fluid-->
</nav>
<script>   
			$(document).ready(function() { 
			    $('.itemNav').click(function(){
            var $target = $('.nav-collapse');
            if($target.hasClass('in')){
              $target.removeClass('in').height(0).css('overflow','hidden');                           
            }
          });			
			});
</script>   
</html>