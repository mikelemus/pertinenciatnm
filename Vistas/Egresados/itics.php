<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../CSS/Footer.css">
    <link rel="stylesheet" href="../CSS/style.css">
    <!-- <link rel="stylesheet" href="css/bootstrap-them.min.css">
      https://www.w3schools.com/tags/att_input_required.asp
      https://www.w3schools.com/html/html_form_input_types.asp
      https://www.w3schools.com/bootstrap/bootstrap_get_started.asp
      https://docs.google.com/forms/d/e/1FAIpQLScsNGOXW2c9SlNSjDNLnB6O-Mv-qWT-Lkq1wkU1KCfcMIVfZQ/viewform
     -->
    <title>Programa de Pertinencia</title>
  </head>
<body>
  <div class="container">
    <header class="row">
      <div class="col-md-12">
        <img src="../imagen/HeaderCIITNE.jpg" class="img-responsive" alt="Imagen responsive" height="160" width="1140">
      </div>
    </header>

    <?php require '../nav.php';?>
    
    <div class="jumbotron">
      <div class="container text-danger text-center">
        <h2>CUESTIONARIO PARA EGRESADOS - ITICS</h2>
      </div>
      <div class="container text-center"> <h3>Sector Productivo y de Servicios</h3></div>
      <div class="container text-center">
        <p>INSTITUTO TECNOLÓGICO SUPERIOR JOSÉ MARIO MOLINA PASQUEL Y HENRÍQUEZ UNIDAD ACADÉMICA PUERTO VALLARTA, JALISCO</p>
      </div>
    </div>

      <form class="" action="../Controles/conInsertarEgresado.php" method="post">
        <div class="jumbotron">
          <div class="form-group">
            <label class="col-sm-12 text-center" ><p>Introduce tu email</p></label>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email" placeholder="email@dominio.com" name="egrCorreo" required>
          </div>
        </div>
        <div class="jumbotron col-sm-12">
          <div class="container text-justify col-sm-8 col-sm-offset-2">
            <button type="button" class="btn btn-danger btn-lg btn-block alert"><H5><label class="glyphicon glyphicon-align-left  glyphicon-info-sign"></label> INSTRUCIONES</H5></button>
          </div>
        </div>
        
        <div class="jumbotron col-sm-12">
          <h2>A. PERFIL DEL PROFESIONAL</h2>
          <p>ITÍCS</p>
          <div class="form-group">
            <label class=" col-sm-10" for="electro_nombre">1.Nombre:</label>
            <input type="text" class="form-control" id="electro_nombre" placeholder="Escriba tus Apellidos Empezando con el Paterno y luego el Materno y por ultimo tu Nombre(s)" name="egrNombreC" required>
          </div>
          <div class="form-group">
            <label class=" col-sm-10" for="electro_fecha">Fecha de Nacimiento:</label>
            <input type="text" class="form-control" id="electro_fecha" placeholder="Direccion Completa" name="egrFecha" required>
          </div>          
          <div class="form-group">
            <label class="control-label">2.Sexo:</label>
            <div class="radio">
            <label><input type="radio" value="Masculino" name="egrSexo" required>Masculino</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Femenino" name="egrSexo">Femenino</label>
            </div>
<!-- ################################################################ -->
          </div>
          <div class="form-group">
            <label class="control-label" for="electro_tipo">3.-Estado Civil:</label>
            <div class="radio">
              <label><input type="radio" value="Casado" name="egrEstadoC" required>Casado</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Soltero" name="egrEstadoC">Soltero</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Otro" name="egrEstadoC">Otros</label>
            </div>
            </div>
        
<!-- ################################################################ -->

          
          <div class="form-group">
            <label class="control-label col-sm-10" for="electro_domicilio">4.-Domicilio: </label>
          <div class="form-group">
            <label class=" " for="electro_domicilo">Calle:</label>
            <input type="text" class="form-control" id="electro_calle"  name="egrCalle" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_domicilo">No.#:</label>
            <input type="text" class="form-control" id="electro_numero"  name="egrNumero" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_domicilo">Colonia:</label>
            <input type="text" class="form-control" id="electro_colonia"  name="egrColonia" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_cp">Codigo Postal:</label>
            <input type="text" class="form-control" id="electro_cp"  name="egrCPostal" required>
          </div>
          </div>
 <!-- ################################################################ -->
       
          <div class="form-group">
            <label class="control-label" for="electro_ciudad">5.-Ciudad:</label>
            <input type="text" class="form-control" id="electro_ciudad"  name="egrCiudad" required>
            </div>
  <!-- ################################################################ -->
       
        <div class="form-group">
            <label class="control-label" for="electro_cel">6.-Telefono Celular</label>
            <input type="text" class="form-control" id="electro_cel"  name="egrCelular" required>
          </div>
  <!-- ################################################################ -->
                    
          <div class="form-group">
            <label class="control-label" for="electro_cel">6.-Telefono de oficina o de casa</label>
            <input type="text" class="form-control" id="electro_celotro"  name="egrTelefono" required>
            </div>
          
  <!-- ################################################################ -->
        
        <div class="form-group">
            <label class="control-label col-sm-2" for="electro_email">7.- Correo electronico:</label>
            <input type="email" class="form-control" id="electro_email" placeholder="email@dominio.com" name="electro_email">
          </div>
  <!-- ################################################################ -->


<div class="form-group">
      <label class="control-label" for="electro_caresp">8.- Carrera de Egreso y Especialidad:</label>
     <input type="text" class="form-control" id="electro_caresp"  name="egrEspecialidad" required>
</div>
  <!-- ################################################################ -->
        
<div class="form-group">
      <label class="control-label">9.- Año de egreso</label>
      <input type="text" class="form-control" id="electro_anio"  name="egrAnioEgreso" required>
</div>
<!-- pregunta 9 -->


 <!-- pregunta 10 -->
<div class="form-group">
      <label class=" control-label">10. Titulado(a):</label>
     <div class="radio">
      <label><input type="radio" value="Titulado" name="egrTitulado" checked>Si, tengo mi Titulo</label>
    </div>
    <div class="radio">
      <label><input type="radio" value="SinTitular" name="egrTitulado">No tengo mi Titulo</label>
    </div>
  </div>

     <!-- pregunta 11 -->
<div class="form-group">
      <label class="control-label"><h4>11. ¿Dominio de idioma Ingles?</h4></label>
    <label class="control-label" for="electro_idioma">En decimas del 10 al 100. Cual es su nivel en porcentaje?:</label>
    <input type="text" class="form-control" id="electro_idioma"  name="egrIngles" required>
    </div>

     <!-- pregunta 12 -->
<div class="form-group">
    <label class="control-label" for="electro_idioma2">12. ¿Domina o habla algun otro idioma? </label>
    <label class="control-label" for="electro_idioma2">En decimas del 10 al 100. Cual es su nivel en porcentaje?:</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="egrOtroI" required>

  </div>

 <!-- pregunta 13 -->
<div class="form-group">
      <label class="control-label"><h4>13. Manejo de Paquetes Computacionales:</h4></label>
        <input type="text" class="form-control" placeholder="Especificar" name="egrPaquetesComp" required>

    </div>
 
 <h2>II. PERTINENCIA Y DISPONIBILIDAD DE MEDIOS Y RECURSOS PARA EL APRENDIZAJE</h2>
<h3>Califique la calidad de la educación profesional proporcionada por el personal docente, así como el Plan de Estudios de la carrera que curso y las condiciones del plantel en cuanto a infraestructura.</h3>
 <!-- pregunta 14 -->
<div class="form-group">
    <label class="control-label" for="respuesta12">II.1 Calidad de los docentes (1 Malo, 5 Excelente)
</label>
             <div class="table-responsive">
              <setfield required>
                <table class=" col-sm-12 table table-striped table-hover" id="tabla">
                  <thead>
                    <tr>
                      <th></th>
                      <th>1</th>
                      <th>2</th>
                      <th>3</th>
                      <th>4</th>
                      <th>5</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Calidad de los docentes</td>
                      <td><label><input type="radio" value="1" name="recCalidad" required></label></td>
                      <td><label><input type="radio" value="2" name="recCalidad"></label></td>
                      <td><label><input type="radio" value="3" name="recCalidad"></label></td>
                      <td><label><input type="radio" value="4" name="recCalidad"></label></td>
                        <td><label><input type="radio" value="5" name="recCalidad"></label></td>
                    </tr>
                    <tr>
                      <td>Plan de Estudios</td>
                      <td><label><input type="radio" value="1" name="recPlan" required></label></td>
                      <td><label><input type="radio" value="2" name="recPlan"></label></td>
                      <td><label><input type="radio" value="3" name="recPlan"></label></td>
                      <td><label><input type="radio" value="4" name="recPlan"></label></td>
                      <td><label><input type="radio" value="5" name="recPlan"></label></td>
                    </tr>
                    <tr>
                      <td>Oportunidad de participar en <br>proyectos de investigacion y<br>desarrollo</td>
                       <td><label ><input type="radio" value="1" name="recInvestigacion" required></label></td>
                       <td><label ><input type="radio" value="2" name="recInvestigacion"></label></td>
                       <td><label ><input type="radio" value="3" name="recInvestigacion"></label></td>
                       <td><label ><input type="radio" value="4" name="recInvestigacion"></label></td>
                        <td><label ><input type="radio" value="5" name="recInvestigacion"></label></td>
                    </tr>
                    <tr>
                     <td>Énfasis que se le prestaba a la<br> investigación dentro del proceso de<br> enseñanza</td>
                     <td><label ><input type="radio" value="1" name="recEnfasis" required></label></td>
                     <td><label ><input type="radio" value="2" name="recEnfasis"></label></td>
                     <td><label ><input type="radio" value="3" name="recEnfasis"></label></td>
                     <td><label ><input type="radio" value="4" name="recEnfasis"></label></td>
                     <td><label ><input type="radio" value="5" name="recEnfasis"></label></td>
                    </tr>
                    <tr>
                      <td>Satisfacción con las<br> condiciones de estudio<br>(infraestructura)</td>
                      <td><label><input type="radio" value="1" name="recSatisfaccion" required></label></td>
                      <td><label><input type="radio" value="2" name="recSatisfaccion"></label></td>
                      <td><label><input type="radio" value="3" name="recSatisfaccion"></label></td>
                      <td><label><input type="radio" value="4" name="recSatisfaccion"></label></td>
                      <td><label><input type="radio" value="5" name="recSatisfaccion"></label></td>
                    </tr>
                  </tbody>
                </table>
              </setfield>
            </div>
  </div>

<!---------------------------------------- -->
        <h2>III. UBICACIÓN LABORAL DEL PROFESIONISTA</h2>
        <h5>Indique a cuál de los siguientes puntos corresponde su situación actual.</h5>

 <!-- pregunta 15 -->
<div class="form-group">
    <label class="control-label">III.1 Actividad a la que se dedica actualmente:</label>
                <div class="radio">
                    <label><input type="radio" value="Programación" name="ubiActividad">Programación</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Redes" name="ubiActividad">Redes</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Mantenimiento" name="ubiActividad" >Mantenimiento</label>
            </div>
            <div class="radio ">
              <label><input type="radio" value="Docencia" name="ubiActividad" >Docencia</label>
            </div>            
            <div class="input-group">
              <span class="input-group-addon">
                <input type="radio" value="Otro" name ="ubiActividad" aria-label="...">Otro
              </span>
              <input type="text" class="form-control" aria-label="...">
            </div>
    </div>

<!-- pregunta 16 -->
<div class="form-group">
        <label class="control-label">16. Si estudia, indicar si es: </label>
    <div class="radio">
        <label><input type="radio" value="Especialidad" name="ubiEstudia">Especialidad</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Maestría" name="ubiEstudia">Maestría</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Doctorado" name="ubiEstudia" >Doctorado</label>
            </div>
            <div class="radio ">
              <label><input type="radio" value="Idiomas" name="ubiEstudia" >Idiomas</label>
            </div>
    </div>

<!-- pregunta 17 -->
<div class="form-group">
    <label class="control-label" for="respuesta17">17. Especialidad e Institucion:</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="ubiEspecialidad" required>
        </div>
    
<!-- pregunta 18 -->
    <div class="form-group">
        <label class="control-label">III.2 ¿Tiempo Transcurrido para obtener el primer empleo después de su carrera?</label>
    <div class="radio">
        <label><input type="radio" value="Antes de egresar" name="ubiTiempoT">Antes de Egresar</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Menos de 6 meses" name="ubiTiempoT">Menos de 6 Meses</label>
            </div>
            <div class="radio">
              <label><input type="radio" value="Entre 6 meses y un año" name="ubiTiempoT" >Entre 6 Meses y un Año</label>
            </div>
            <div class="radio ">
              <label><input type="radio" value="Más de un año" name="ubiTiempoT" >Más de un año</label>
            </div>
    </div>
    <!-- pregunta 19 -->
    <div class="form-group">
        <label class="control-label">III.3 ¿Medio para Obtener el Empleo?</label>
    <div class="radio">
        <label><input type="radio" value="Bolsa del trabajo del plantel" name="ubiMedioE">Bolsa del trabajo del plantel</label>
            </div>
            <div class="radio">
                <label><input type="radio" value="Contactos personales" name="ubiMedioE">Contactos personales</label>
            </div>
            <div class="radio">
                <label><input type="radio" value="Practicas profesionales" name="ubiMedioE" >Practicas profesionales</label>
            </div>
            <div class="radio ">
                <label><input type="radio" value="Medios masivos de comunicacion" name="ubiMedioE" >Medios masivos de comunicación</label>
            </div>
            <div class="radio ">
                <label><input type="radio" value="Otros" name="ubiMedioE" >Otros</label>
            </div>
    </div>
    
        <!-- pregunta 20 -->
    <div class="form-group">
        <label class="control-label">III.4 ¿Requisitos de contratación?</label>
    <div class="radio">
        <label><input type="radio" name="ubiRequisitos" value="Competencias Laborales" >Competencias Laborales</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="ubiRequisitos" value="Titulo Profesional" >Titulo Profesional</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="ubiRequisitos" value="Examen de seleccion" >Examen de seleccion</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="ubiRequisitos" value="Idioma Extranjero" >Idioma Extranjero</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="ubiRequisitos" value="Habilidades" >Habilidades</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="ubiRequisitos" value="Valores" >Valores</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="ubiRequisitos" value="Actitudes y Aptitudes" >Actitudes y Aptitudes</label>
            </div>
    </div>
    <!-- pregunta 21 -->
       <div class="form-group">
        <label class="control-label">III.5 ¿Idioma extranjero que utiliza en su trabajo?</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="ubiIdioma" value="Ingles" >Ingles</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIdioma" value="Frances" >Frances</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIdioma" value="Aleman" >Aleman</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIdioma" value="Japones" >Japones</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIdioma" value="Otros" >Otros</label>
            </div>
    </div>
    </div>
               <!-- pregunta 22 -->
       <div class="form-group">
        <label class="control-label">III.6 ¿En qué proporción utiliza en el desempeño de sus actividades laborales cada una de las habilidades ?del idioma extranjero:</label>
           <label class="control-label">Porcentaje %</label>
     <div class="table-responsive">
     <table id="tabla" class=" col-sm-12 table table-striped table-hover">
    <thead>
      <tr>
        <th></th>
        <th>20%</th>
        <th>40%</th>
        <th>60%</th>
        <th>80%</th>
        <th>100%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Hablar</td>
        <td><label ><input type="radio" name="ubiPHablar" value="20" required></label></td>
        <td><label ><input type="radio" name="ubiPHablar" value="40" ></label></td>
        <td><label ><input type="radio" name="ubiPHablar" value="60" ></label></td>
        <td><label ><input type="radio" name="ubiPHablar" value="80" ></label></td>
        <td><label ><input type="radio" name="ubiPHablar" value="100"></label></td>
      </tr>
      <tr>
        <td>Escribir</td>
        <td><label ><input type="radio" name="ubiPEscribir" value="20" required></label></td>
        <td><label ><input type="radio" name="ubiPEscribir" value="40" ></label></td>
        <td><label ><input type="radio" name="ubiPEscribir" value="60" ></label></td>
        <td><label ><input type="radio" name="ubiPEscribir" value="80" ></label></td>
        <td><label ><input type="radio" name="ubiPEscribir" value="100" ></label></td>
      </tr>

      <tr>
       <td>Leer</td>
        <td><label ><input type="radio" name="ubiPLeer" value="20" required></label></td>
        <td><label ><input type="radio" name="ubiPLeer" value="40" ></label></td>
        <td><label ><input type="radio" name="ubiPLeer" value="60" ></label></td>
        <td><label ><input type="radio" name="ubiPLeer" value="80" ></label></td>
        <td><label ><input type="radio" name="ubiPLeer" value="100" ></label></td>
      </tr>
      <tr>
        <td>Escuchar</td>
        <td><label ><input type="radio" name="ubiPEscuchar" value="20" required></label></td>
        <td><label ><input type="radio" name="ubiPEscuchar" value="40" ></label></td>
        <td><label ><input type="radio" name="ubiPEscuchar" value="60" ></label></td>
        <td><label ><input type="radio" name="ubiPEscuchar" value="80" ></label></td>
        <td><label ><input type="radio" name="ubiPEscuchar" value="100" ></label></td>
      </tr>
    </tbody>
  </table>
</div>
           </div>
           
               <!-- pregunta 23 -->
       <div class="form-group">
        <label class="control-label">III.7 ¿Antigüedad en el empleo?</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="ubiAntiguedad" value="Menos de un año" >Menos de un año</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiAntiguedad" value="Un año" >Un año</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiAntiguedad" value="Dos años" >Dos años</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiAntiguedad" value="Mas de tres años" >Mas de tres años</label>
            </div>
           </div>
    </div>
                          <!-- pregunta 24 -->
       <div class="form-group">
        <label class="control-label">III.8 ¿Ingreso (salario mínimo diario)?</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="ubiIngreso" value="Menos de 6000" >Menos de 6000</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIngreso" value="Entre 6000 y 11000" >Entre 6000 y 11000</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIngreso" value="Entre 11000 y 20000" >Entre 11000 y 20000</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiIngreso" value="Mas de 20000" >Mas de 20000</label>
            </div>
           </div>
    </div>
           
                         <!-- pregunta 25 -->
       <div class="form-group">
        <label class="control-label">III.9 ¿Nivel jerárquico en el trabajo?</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="ubiNivel" value="Técnico" >Técnico</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiNivel" value="Supervisor" >Supervisor</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiNivel" value="Jefe de área" >Jefe de área</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiNivel" value="Directivo" >Directivo</label>
            </div>
                                    <div class="radio ">
              <label><input type="radio" name="ubiNivel" value="Empresario">Empresario</label>
            </div>
                                    <div class="radio ">
              <label><input type="radio" name="ubiNivel" value="Funcionario" >Funcionario</label>
            </div>
           </div>
    </div>
                            <!-- pregunta 26 -->
       <div class="form-group">
        <label class="control-label">III.10 ¿Condición de Trabajo?</label>
        <div class="radio">
            <div class="radio">
                <label><input type="radio" name="ubiCondicion" value="Base" >Base</label>
            </div>
                        <div class="radio">
              <label><input type="radio" name="ubiCondicion" value="Eventual" >Eventual</label>
            </div>
                        <div class="radio">
              <label><input type="radio" name="ubiCondicion" value="Contrato" >Contrato</label>
            </div>
                        <div class="radio">
              <label><input type="radio" name="ubiCondicion" value="Otro" >Otro</label>
            </div>
           </div>
    </div>
                             <!-- pregunta 27 -->
       <div class="form-group">
        <label class="control-label">III.11 ¿Relación del trabajo con su área de Formación Profesional?</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="ubiRelacion" value="0" >0%</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiRelacion" value="20" >20%</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiRelacion" value="40" >40%</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="ubiRelacion" value="60" >60%</label>
            </div>
                            <div class="radio ">
              <label><input type="radio" name="ubiRelacion" value="80" >80%</label>
            </div>
                              <div class="radio ">
              <label><input type="radio" name="ubiRelacion" value="100" >100%</label>
            </div>
           </div>
    </div>
                                    <!-- pregunta 28 -->
       <div class="form-group">
        <label class="control-label">III.12 ¿Datos de la empresa u organismo?</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="empDatos" value="publico">Público</label> </label>
            </div>
                        <div class="radio ">
                <label><input type="radio" name="empDatos" value="privado">Privado</label>
            </div>
                        <div class="radio ">
                <label><input type="radio" name="empDatos" value="social">Social</label>
            </div>
           </div>
    </div>
        <!-- pregunta 29 -->
        <div class="form-group">
      <label class="control-label" for="electro_giroact">¿Giro o actividad principal de la empresa u Organismo?</label>
        <input type="text" class="form-control"  name="empGiro" required>
    </div>
                <!-- pregunta 30 -->
        <div class="form-group">
      <label class="control-label" for="electro_razonsocial">Razon Social:</label>
        <input type="text" class="form-control"  name="empRazonS" required>
    </div>
        
            <!-- pregunta 31 -->
                  <div class="form-group">
            <div class="form-group">
            <label class="" for="electro_domiciloemp">Domicilio: </label>
            <input type="text" class="form-control" id="electro_domiciloemp" name="electro_domiciloemp" disabled="true">
                </div>
          <div class="form-group">
            <label class=" " for="electro_calleemp">Calle:</label>
            <input type="text" class="form-control" id="electro_calleemp"  name="empCalle" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_numeroemp">No.#:</label>
            <input type="text" class="form-control" id="electro_numeroemp"  name="empNumero" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_coloniaemp">Colonia:</label>
            <input type="text" class="form-control" id="electro_coloniaemp"  name="empColonia" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_cpemp">Codigo Postal:</label>
            <input type="text" class="form-control" id="electro_cpemp"  name="empCodigoP" required>
          </div>
          </div>
        <!-- pregunta 32 -->
            <div class="form-group">
      <label class="control-label" for="electro_ciudademp">Ciudad:</label>
        <input type="text" class="form-control"  name="empCiudad" required>
    </div>
                <!-- pregunta 32 -->
            <div class="form-group">
      <label class="control-label" for="electro_municipioemp">Municipio:</label>
        <input type="text" class="form-control"  name="empMunicipio" required>
    </div>
                        <!-- pregunta 33 -->
            <div class="form-group">
      <label class="control-label" for="electro_estadoemp">Estado:</label>
        <input type="text" class="form-control"  name="empEstado" required>
    </div>
                        <!-- pregunta 34 -->
            <div class="form-group">
      <label class="control-label" for="electro_telefonos">Telefonos:(01 -)</label>
        <input type="text" class="form-control"  name="empTelefono" required>
    </div>
                        <!-- pregunta 35 -->
            <div class="form-group">
      <label class="control-label" for="electro_telmix">Tel y Ext. Fax E-mail:</label>
        <input type="text" class="form-control"  name="empExtension" required>
    </div>
                         <!-- pregunta 36 -->
            <div class="form-group">
      <label class="control-label" for="electro_webemp">Pagina Web:</label>
        <input type="text" class="form-control"  name="empPagina" required>
    </div>
                                 <!-- pregunta 37 -->
            <div class="form-group">
      <label class="control-label" for="electro_nombrejefe">Nombre y puesto del Jefe Inmediato:</label>
        <input type="text" class="form-control"  name="empJefe" required>
    </div>
                                            <!-- pregunta 38 -->
       <div class="form-group">
        <label class="control-label">III.13 Sector Económico de la Empresa u Organización</label>
           <label>Sector Primario</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="empSectorP" value="Agroindustria" >Agroindustria</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empSectorP" value="Pesquero" >Pesquero</label>
            </div>
                        <div class="radio ">
                <label><input type="radio" name="empSectorP" value="Otros" >Otros</label>
            </div>
           </div>
           
                     <label>Sector Secundario</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="empSectorS" value="Industrial" >Industrial</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empSectorS" value="Construccion" >Construcción</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empSectorS" value="Otros" >Otros</label>
            </div>
           </div>
           
           
                     <label>Sector Terciario</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="empSectorT" value="Hotelero" >Hotelero</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empSectorT" value="Restaurante" >Restaurante</label>
            </div>
                 <div class="radio ">
              <label><input type="radio" name="empSectorT" value="Peritajes" >Peritajes</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empSectorT" value="Otros" >Otros</label>
            </div>
           </div>
    </div>
        
                    <!-- pregunta 39 -->
       <div class="form-group">
        <label class="control-label">III.14 Tamaño de la empresa u organización:</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="empTamano" value="MicroEmpresa (10-30)" >MicroEmpresa (10-30)</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empTamano" value="Pequeña(31-100)" >Pequeña(31-100)</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="empTamano" value="Mediana (101-500)" >Mediana (101-500)</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="empTamano" value="Grande(mas de 500)" >Grande(mas de 500)</label>
            </div>
           </div>
        </div>
        <h2>IV. DESEMPEÑO PROFESIONAL (COHERENCIA ENTRE LA FORMACIÓN Y EL TIPO DE EMPLEO)</h2>
        <h4>Marcar los campos que correspondan a su trayectoria personal</h4>
        
        
                            <!-- pregunta 40 -->
       <div class="form-group">
        <label class="control-label">IV.1 Eficiencia para realizar las actividades laborales, en relación con su formación profesional</label>
        <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="desEficiencia" value="Muy eficiente" >Muy Eficiente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="desEficiencia" value="Eficiente" >Eficiente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="desEficiencia" value="Poco Eficiente" >Poco Eficiente</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="desEficiencia" value="Deficiente" >Deficiente</label>
            </div>
           </div>
        </div>
                                <!-- pregunta 41 -->
       <div class="form-group">
        <label class="control-label">IV.2 ¿Cómo califica su formación profesional con respecto a su desempeño laboral?</label>
         <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="desCalifica" value="Excelente" >Excelente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="desCalifica" value="Bueno" >Bueno</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="desCalifica" value="Regular" >Regular</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="desCalifica" value="Malo" >Malo</label>
            </div>
                    <div class="radio ">
              <label><input type="radio" name="desCalifica" value="Pesimo" >Pésimo</label>
            </div>
           </div>
        </div>
        
                                       <!-- pregunta 42 -->
       <div class="form-group">
        <label class="control-label">IV.3 Utilidad de las prácticas profesionales para su desarrollo laboral y profesional</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="desUtilidad" value="Excelente" >Excelente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="desUtilidad" value="Bueno" >Bueno</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="desUtilidad" value="Regular">Regular</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="desUtilidad" value="Malo" >Malo</label>
            </div>
                    <div class="radio ">
              <label><input type="radio" name="desUtilidad" value="Pesimo" >Pésimo</label>
            </div>
           </div>
        </div>
        
                                 <!-- pregunta 43 -->
       <div class="form-group">
        <label class="control-label">IV.4 Aspectos que valora la empresa u organismo para la contratación de profesionistas: (1 Poco, 5 Mucho)</label>
     <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1. Habilidad para crear y administrar cambios</td>
        <td><label ><input type="radio" name="desAspecto1" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto1" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto1" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto1" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto1" value="5"></label></td>
      </tr>
              <tr>
        <td>2. Alinear la tecnologia con la organizacion</td>
        <td><label ><input type="radio" name="desAspecto2" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto2" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto2" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto2" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto2" value="5"></label></td>
      </tr>
              <tr>
        <td>3. Experiencia Laboral</td>
        <td><label ><input type="radio" name="desAspecto3" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto3" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto3" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto3" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto3" value="5"></label></td>
      </tr>
              <tr>
        <td>4. Competencia Laboral</td>
        <td><label ><input type="radio" name="desAspecto4" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto4" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto4" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto4" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto4" value="5"></label></td>
      </tr>
              <tr>
        <td>5. Posicionamiento de la Institucion de Egreso</td>
        <td><label ><input type="radio" name="desAspecto5" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto5" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto5" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto5" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto5" value="5"></label></td>
      </tr>
              <tr>
        <td>6. Conocimiento de Idiomas Extranjeros</td>
        <td><label ><input type="radio" name="desAspecto6" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto6" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto6" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto6" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto6" value="5"></label></td>
      </tr>
              <tr>
        <td>7. Recomendaciones/referencia</td>
        <td><label ><input type="radio" name="desAspecto7" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto7" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto7" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto7" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto7" value="5"></label></td>
      </tr>
              <tr>
        <td>8. Personalidad/Actitudes</td>
        <td><label ><input type="radio" name="desAspecto8" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto8" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto8" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto8" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto8" value="5"></label></td>
      </tr>
              <tr>
        <td>9. Capacidad de liderazgo</td>
        <td><label ><input type="radio" name="desAspecto9" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto9" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto9" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto9" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto9" value="5"></label></td>
      </tr>
              <tr>
        <td>10. Transferencia de Conocimiento y tecnologia al campo laboral</td>
        <td><label ><input type="radio" name="desAspecto10" value="1" ></label></td>
        <td><label ><input type="radio" name="desAspecto10" value="2"></label></td>
        <td><label ><input type="radio" name="desAspecto10" value="3"></label></td>
        <td><label ><input type="radio" name="desAspecto10" value="4"></label></td>
        <td><label ><input type="radio" name="desAspecto10" value="5"></label></td>
      </tr>
    </tbody>
  </table>
</setfield>
        </div>
                    
<!-- ################################################ -->      
        
                    <!-- pregunta 44 -->
         <h2>V. EXPECTATIVAS DE DESARROLLO, SUPERACIÓN PROFESIONAL Y DE ACTUALIZACIÓN:</h2>
        <h4>V.1 ACTUALIZACION DE CONOCIMIENTOS</h4>
       <div class="form-group">
        <label class="control-label">¿Le gustaría tomar cursos de actualización en Electromecánica?</label>
         <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="espCursos" value="Si" >Si</label>
            </div>
            <div class="radio ">
                <label><input type="radio" name="espCursos" value="No" >No</label>
            </div>
           </div>
        </div>
        
         <!-- pregunta 45 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_upd">¿Cuál?</label>
            <input type="text" class="form-control" id="electro_upd"  name="espCursoCual">
          </div>
          </div>
        <!-- pregunta 46 -->
               <div class="form-group">
        <label class="control-label">¿Le gustaría tomar un Posgrado en Ciencias en Ingeniería Industrial?</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="espPostgrado" value="Si" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="espPostgrado" value="No" >No</label>
            </div>
           </div>
        </div>
                <!-- pregunta 47 -->
               <div class="form-group">
        <label class="control-label">¿Le gustaría capacitarse en ingeniería mecatrónica?</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="espCapacitarse" value="Si" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="espCapacitarse" value="No" >No</label>
            </div>
           </div>
        </div>
                 <!-- pregunta 48 -->
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_part">VI. PARTICIPACIÓN SOCIAL DEL PROFESIONISTA:</label>
            <input type="text" class="form-control" id="electro_part"  name="espParticipacion" placeholder="(Opcional)">
          </div>
          </div>
                        <!-- pregunta 49 -->
               <div class="form-group">
        <label class="control-label">VI.1 ¿Pertenece a organizaciones sociales:? </label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="espPOrgSociales" value="Si" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="espPOrgSociales" value="No" >No</label>
            </div>
           </div>
        </div>
                 <!-- pregunta 50 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_soc">¿Cuáles?</label>
            <input type="text" class="form-control" id="electro_soc"  name="espSocialesDesc">
          </div>
          </div>
                               <!-- pregunta 51 -->
               <div class="form-group">
        <label class="control-label">VI.2 ¿Pertenece a organizaciones profesionistas:? </label>
         <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="espPOrgProfesionistas" value="Si" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="espPOrgProfesionistas" value="No" >No</label>
            </div>
           </div>
        </div>
                         <!-- pregunta 52 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_profesionales">¿Cuáles?</label>
            <input type="text" class="form-control" id="electro_profesionales"  name="espProfesionistasDesc">
          </div>
          </div>
                   <!-- pregunta 53 -->
               <div class="form-group">
        <label class="control-label">VI.3 ¿Pertenece a la asociación de egresados? </label>
         <div class="radio">
            <div class="radio ">
                <label><input type="radio" name="espPOrgEgresados" value="Si" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="espPOrgEgresados" value="No" >No</label>
            </div>
           </div>
        </div>
                          <!-- pregunta 52 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_egresados">¿Cuál?</label>
            <input type="text" class="form-control" id="electro_egresados"  name="espEgresadosDesc">
          </div>
          </div>
        

<!-- ###################################################### -->

<div class="form-group">
      <label class="control-label" for="nombre">COMENTARIOS Y SUGERENCIAS:</label>
        <textarea class="form-control" id="det" placeholder="Quejas o comentarios" name="espComentarios"></textarea>
 </div>
<div class="form-group">
      <label class="control-label" for="electro_estadoemp">Estado:</label>
        <input type="text" class="form-control"  name="espRecomendacion" required>
    </div>


                                 
<!-- Recuperar código Aquíiiiiiiiii -->        

  <div class="form-group">
   <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
  </div>
</form>
</div>

</div>
<footer class="row">
      <div class="col-md-12">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </footer>

<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
 <!-- bootbox code -->
    <script src="../js/bootbox.min.js"></script>
    <script>
        $(document).on("click", ".alert", function(e) {
          bootbox.alert({
    message:"Por favor lea cuidadosamente y conteste este cuestionario de la siguiente manera, según sea el caso:<br><br>1. En el caso de preguntas cerradas, marque la que considere aprrb4iada de esta manera:(x) <br><br>2. En las preguntas de valoración se utiliza la escala del 1 al 5 en la que 1 es “muy malo” y 5 es “muy bueno”.<br><br>3. En los casos de preguntas abiertas dejamos un espacio para que usted escriba con mayúscula una respuesta (______________________________).<br>Si el espacio para su respuesta no es suficiente, por favor añada una hoja adicional al cuestionario.<br><br>4. Al final anexamos un inciso para comentarios y sugerencias, le agradeceremos anote ahí lo que considere prudente para mejorar nuestro sistema educativo o bien los temas que, a su juicio, omitimos en el cuestionario.<br><br>Gracias por su gentil colaboración.",
    backdrop: true,
    size: 'large'
});
        });
    </script>
</body>
</html>
