<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../CSS/Footer.css">
    <link rel="stylesheet" href="../CSS/style.css">
    <!-- <link rel="stylesheet" href="css/bootstrap-them.min.css">
      https://www.w3schools.com/tags/att_input_required.asp
      https://www.w3schools.com/html/html_form_input_types.asp
      https://www.w3schools.com/bootstrap/bootstrap_get_started.asp
      https://docs.google.com/forms/d/e/1FAIpQLScsNGOXW2c9SlNSjDNLnB6O-Mv-qWT-Lkq1wkU1KCfcMIVfZQ/viewform
     -->
    <title>Programa de Pertinencia</title>
  </head>
<body>
  <div class="container">
    <header class="row">
      <div class="col-md-12">
        <img src="../imagen/HeaderCIITNE.jpg" class="img-responsive" alt="Imagen responsive" height="160" width="1140">
      </div>
    </header>

    <?php require '../nav.php';?>
    
    <div class="jumbotron">
      <div class="container text-danger text-center">
        <h2>CUESTIONARIO PARA EGRESADOS - INGENIERÍA EN INDUSTRIAS ALIMENTARIAS</h2>
      </div>
      <div class="container text-center"> <h3>Sector Productivo y de Servicios</h3></div>
      <div class="container text-center">
        <p>INSTITUTO TECNOLÓGICO SUPERIOR JOSÉ MARIO MOLINA PASQUEL Y HENRÍQUEZ UNIDAD ACADÉMICA PUERTO VALLARTA, JALISCO</p>
      </div>
    </div>

    <form class="" action="/action_page.php">
        <div class="jumbotron">
          <div class="form-group">
            <label class="col-sm-12 text-center" ><p>Introduce tu email</p></label>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email" placeholder="email@dominio.com" name="email" required>
          </div>
        </div>
        <div class="jumbotron col-sm-12">
          <div class="container text-justify col-sm-8 col-sm-offset-2">
            <button type="button" class="btn btn-danger btn-lg btn-block alert"><H5><label class="glyphicon glyphicon-align-left  glyphicon-info-sign"></label> INSTRUCIONES</H5></button>
          </div>
        </div>
        
        <div class="jumbotron col-sm-12">
          <h2>A. PERFIL DEL PROFESIONAL</h2>
          <p>Electromecanica</p>
          <div class="form-group">
            <label class=" col-sm-10" for="electro_nombre">1.Nombre:</label>
            <input type="text" class="form-control" id="electro_nombre" placeholder="Escriba tus Apellidos Empezando con el Paterno y luego el Materno y por ultimo tu Nombre(s)" name="Empresa_nombre" required>
          </div>
          <div class="form-group">
            <label class=" col-sm-10" for="electro_fecha">Fecha de Nacimiento:</label>
            <input type="text" class="form-control" id="electro_fecha" placeholder="Direccion Completa" name="electro_fecha" required>
          </div>
          <!--<div class="form-group">
            <label class="control-label col-sm-2" for="Empresa_ciudad">Ciudad:</label>
            <input type="text" class="form-control" id="Empresa_ciudad" placeholder="Ciudad" name="Empresa_ciudad" required>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="Empresa_municipio">Municipio:</label>
            <input type="text" class="form-control" id="Empresa_municipio" placeholder="Municipio" name="Empresa_municipio" required>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="Empresa_estado">Estado:</label>
            <input type="rfc" class="form-control" id="Empresa_estado" placeholder="Estado" name="Empresa_estado" required>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="Empresa_tel">Teléfono:</label>
            <input type="number" "number_format(, '.', ',')" class="form-control" id="Empresa_tel" placeholder="Numero a 10 digitos" name="Empresa_tel" required>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="Empresa_mail">Email de la Empresa:</label>
            <input type="email" class="form-control" id="Empresa_mail" placeholder="email@dominio.com" name="Empresa_mail" required>
          </div>-->
 <!-- ################################################################ -->           
          <div class="form-group">
            <label class="control-label">2.Sexo:</label>
            <div class="radio">
            <label><input type="radio" name="rb2" required>Masculino</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb2">Femenino</label>
            </div>
<!-- ################################################################ -->
          </div>
          <div class="form-group">
            <label class="control-label" for="electro_tipo">3.-Estado Civil:</label>
            <div class="radio">
              <label><input type="radio" name="rb3" required>Casado</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb3">Soltero</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb3">Otros</label>
            </div>
            </div>
        
<!-- ################################################################ -->

          
          <div class="form-group">
            <label class="control-label col-sm-10" for="electro_domicilio">4.-Domicilio: </label>
          <div class="form-group">
            <label class=" " for="electro_domicilo">Calle:</label>
            <input type="text" class="form-control" id="electro_calle"  name="electro_calle" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_domicilo">No.#:</label>
            <input type="text" class="form-control" id="electro_numero"  name="electro_numero" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_domicilo">Colonia:</label>
            <input type="text" class="form-control" id="electro_colonia"  name="electro_colonia" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_cp">Codigo Postal:</label>
            <input type="text" class="form-control" id="electro_cp"  name="electro_cp" required>
          </div>
          </div>
 <!-- ################################################################ -->
       
          <div class="form-group">
            <label class="control-label" for="electro_ciudad">5.-Ciudad:</label>
            <input type="text" class="form-control" id="electro_ciudad"  name="electro_ciudad" required>
            </div>
  <!-- ################################################################ -->
       
        <div class="form-group">
            <label class="control-label" for="electro_cel">6.-Telefono Celular</label>
            <input type="text" class="form-control" id="electro_cel"  name="electro_cel" required>
          </div>
  <!-- ################################################################ -->
                    
          <div class="form-group">
            <label class="control-label" for="electro_cel">6.-Telefono de oficina o de casa</label>
            <input type="text" class="form-control" id="electro_celotro"  name="electro_celotro" required>
            </div>
          
  <!-- ################################################################ -->
        
        <div class="form-group">
            <label class="control-label col-sm-2" for="electro_email">7.- Correo electronico:</label>
            <input type="email" class="form-control" id="electro_email" placeholder="email@dominio.com" name="electro_email" required>
          </div>
  <!-- ################################################################ -->


<div class="form-group">
      <label class="control-label" for="electro_caresp">8.- Carrera de Egreso y Especialidad:</label>
     <input type="text" class="form-control" id="electro_caresp"  name="electro_caresp" required>
</div>
  <!-- ################################################################ -->
        
<div class="form-group">
      <label class="control-label">9.- Año de egreso</label>
     
</div>
<!-- pregunta 9 -->


 <!-- pregunta 10 -->
<div class="form-group">
      <label class=" control-label">10. Titulado(a):</label>
     <div class="radio">
      <label><input type="radio" name="rb10">Si, tengo mi Titulo</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="rb10">No tengo mi Titulo</label>
    </div>
  </div>

     <!-- pregunta 11 -->
<div class="form-group">
      <label class="control-label"><h4>11. ¿Dominio de idioma Ingles?</h4></label>
    <label class="control-label" for="electro_idioma">En decimas del 10 al 100. Cual es su nivel en porcentaje?:</label>
    <input type="text" class="form-control" id="electro_idioma"  name="electro_idioma" required>
    </div>

     <!-- pregunta 12 -->
<div class="form-group">
    <label class="control-label" for="electro_idioma2">12. ¿Domina o habla algun otro idioma? </label>
    <label class="control-label" for="electro_idioma2">En decimas del 10 al 100. Cual es su nivel en porcentaje?:</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="electro_idioma2" required>

  </div>

 <!-- pregunta 13 -->
<div class="form-group">
      <label class="control-label"><h4>13. Manejo de Paquetes Computacionales:</h4></label>
        <input type="text" class="form-control" placeholder="Especificar" name="electro_compupaq" required>

    </div>

        
<h2>II. PERTINENCIA Y DISPONIBILIDAD DE MEDIOS Y RECURSOS PARA EL APRENDIZAJE</h2>
<h3>Califique la calidad de la educación profesional proporcionada por el personal docente, así como el Plan de Estudios de la carrera que curso y las condiciones del plantel en cuanto a infraestructura.</h3>
 <!-- pregunta 14 -->
<div class="form-group">
    <label class="control-label" for="respuesta12">II.1 Calidad de los docentes (1 Malo, 5 Excelente)
</label>
                <div class="table-responsive">
              <setfield required>
                <table class=" col-sm-12 table table-striped table-hover" id="tabla">
                  <thead>
                    <tr>
                      <th></th>
                      <th>1</th>
                      <th>2</th>
                      <th>3</th>
                      <th>4</th>
                      <th>5</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Calidad de los docentes</td>
                      <td><label><input type="radio" name="rb14-1" required></label></td>
                      <td><label><input type="radio" name="rb14-1"></label></td>
                      <td><label><input type="radio" name="rb14-1"></label></td>
                      <td><label><input type="radio" name="rb14-1"></label></td>
                        <td><label><input type="radio" name="rb14-1"></label></td>
                    </tr>
                    <tr>
                      <td>Plan de Estudios</td>
                      <td><label><input type="radio" name="rb14-2" required></label></td>
                      <td><label><input type="radio" name="rb14-2"></label></td>
                      <td><label><input type="radio" name="rb14-2"></label></td>
                      <td><label><input type="radio" name="rb14-2"></label></td>
                      <td><label><input type="radio" name="rb14-2"></label></td>
                    </tr>
                    <tr>
                      <td>Oportunidad de participar en <br>proyectos de investigacion y<br>desarrollo</td>
                       <td><label ><input type="radio" name="rb14-3" required></label></td>
                       <td><label ><input type="radio" name="rb14-3"></label></td>
                       <td><label ><input type="radio" name="rb14-3"></label></td>
                       <td><label ><input type="radio" name="rb14-3"></label></td>
                        <td><label ><input type="radio" name="rb14-3"></label></td>
                    </tr>
                    <tr>
                     <td>Énfasis que se le prestaba a la<br> investigación dentro del proceso de<br> enseñanza</td>
                     <td><label ><input type="radio" name="rb14-4"r equired></label></td>
                     <td><label ><input type="radio" name="rb14-4"></label></td>
                     <td><label ><input type="radio" name="rb14-4"></label></td>
                     <td><label ><input type="radio" name="rb14-4"></label></td>
                     <td><label ><input type="radio" name="rb14-4"></label></td>
                    </tr>
                    <tr>
                      <td>Satisfacción con las<br> condiciones de estudio<br>(infraestructura)</td>
                      <td><label><input type="radio" name="rb14-5" required></label></td>
                      <td><label><input type="radio" name="rb14-5"></label></td>
                      <td><label><input type="radio" name="rb14-5"></label></td>
                      <td><label><input type="radio" name="rb14-5"></label></td>
                      <td><label><input type="radio" name="rb14-5"></label></td>
                    </tr>
                  </tbody>
                </table>
              </setfield>
            </div>
  </div>
<!-- ################################################ -->      
        <h2>III. UBICACIÓN LABORAL DEL PROFESIONISTA</h2>
        <h5>Indique a cuál de los siguientes puntos corresponde su situación actual.</h5>

 <!-- pregunta 15 -->
<div class="form-group">
    <label class="control-label">III.1 Actividad a la que se dedica actualmente:</label>
                <div class="radio">
              <label><input type="radio" name="rb15">Programación</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb15">Redes</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb15" >Mantenimiento</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb15" >Docencia</label>
            </div>
            <!--<div class="radio ">
              <label><input type="radio" name="rb15" >Otro</label>
            </div>-->
            <div class="input-group">
              <span class="input-group-addon">
                <input type="radio" name ="rb15" aria-label="...">Otro
              </span>
              <input type="text" class="form-control" aria-label="...">
            </div>
    </div>

<!-- pregunta 16 -->
<div class="form-group">
        <label class="control-label">16. Si estudia, indicar si es: </label>
    <div class="radio">
              <label><input type="radio" name="rb16">Especialidad</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb16">Maestria</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb16" >Doctorado</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb16" >Idiomas</label>
            </div>
    </div>

<!-- pregunta 17 -->
<div class="form-group">
    <label class="control-label" for="respuesta17">17. Especialidad e Institucion:</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="respuesta17" required>
        </div>
    
<!-- pregunta 18 -->
    <div class="form-group">
        <label class="control-label">III.2 ¿Tiempo Transcurrido para obtener el primer empleo después de su carrera?</label>
    <div class="radio">
              <label><input type="radio" name="rb18">Antes de Egresar</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb18">Menos de 6 Meses</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb18" >Entre 6 Meses y un Año</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb18" >Mas de un Año</label>
            </div>
    </div>
    <!-- pregunta 19 -->
    <div class="form-group">
        <label class="control-label">III.3 ¿Medio para Obtener el Empleo?</label>
    <div class="radio">
              <label><input type="radio" name="rb19">Bolsa del trabajo del plantel</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb19">Contactos personales</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb19" >Practicas profesionales</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb19" >Medios Masivos de Comunicacion</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb19" >Otros</label>
            </div>
    </div>
    
        <!-- pregunta 20 -->
    <div class="form-group">
        <label class="control-label">III.4 ¿Requisitos de contratación?</label>
    <div class="radio">
              <label><input type="radio" name="rb20">Competencias Laborales</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb20">Titulo Profesional</label>
            </div>
            <div class="radio">
              <label><input type="radio" name="rb20" >Examen de seleccion</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb20" >Idioma Extranjero</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb20" >Habilidades</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb20" >Valores</label>
            </div>
            <div class="radio ">
              <label><input type="radio" name="rb20" >Actitudes y Aptitudes</label>
            </div>
    </div>
    <!-- pregunta 21 -->
       <div class="form-group">
        <label class="control-label">III.5 ¿Idioma extranjero que utiliza en su trabajo?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb21" >Ingles</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb21" >Frances</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb21" >Aleman</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb21" >Japones</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb21" >Otros</label>
            </div>
    </div>
    </div>
               <!-- pregunta 22 -->
       <div class="form-group">
        <label class="control-label">III.6 ¿En qué proporción utiliza en el desempeño de sus actividades laborales cada una de las habilidades ?del idioma extranjero:</label>
           <label class="control-label">Porcentaje %</label>
     <div class="table-responsive">
     <table id="tabla" class=" col-sm-12 table table-striped table-hover">
    <thead>
      <tr>
        <th></th>
        <th>20%</th>
        <th>40%</th>
        <th>60%</th>
        <th>80%</th>
        <th>100%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Hablar</td>
        <td><label ><input type="radio" name="rb22-1"required></label></td>
        <td><label ><input type="radio" name="rb22-1"></label></td>
        <td><label ><input type="radio" name="rb22-1"></label></td>
        <td><label ><input type="radio" name="rb22-1"></label></td>
        <td><label ><input type="radio" name="rb22-1"></label></td>
      </tr>
      <tr>
        <td>Escribir</td>
        <td><label ><input type="radio" name="rb22-2"required></label></td>
        <td><label ><input type="radio" name="rb22-2"></label></td>
        <td><label ><input type="radio" name="rb22-2"></label></td>
        <td><label ><input type="radio" name="rb22-2"></label></td>
        <td><label ><input type="radio" name="rb22-2"></label></td>
      </tr>

      <tr>
       <td>Leer</td>
        <td><label ><input type="radio" name="rb22-3"required></label></td>
        <td><label ><input type="radio" name="rb22-3"></label></td>
        <td><label ><input type="radio" name="rb22-3"></label></td>
        <td><label ><input type="radio" name="rb22-3"></label></td>
        <td><label ><input type="radio" name="rb22-3"></label></td>
      </tr>
      <tr>
        <td>Escuchar</td>
        <td><label ><input type="radio" name="rb22-4"required></label></td>
        <td><label ><input type="radio" name="rb22-4"></label></td>
        <td><label ><input type="radio" name="rb22-4"></label></td>
        <td><label ><input type="radio" name="rb22-4"></label></td>
        <td><label ><input type="radio" name="rb22-4"></label></td>
      </tr>
    </tbody>
  </table>
</div>
           </div>
           
               <!-- pregunta 23 -->
       <div class="form-group">
        <label class="control-label">III.7 ¿Antigüedad en el empleo?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb23" >Menos de un año</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb23" >Un año</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb23" >Dos años</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb23" >Mas de tres años</label>
            </div>
           </div>
    </div>
                          <!-- pregunta 24 -->
       <div class="form-group">
        <label class="control-label">III.8 ¿Ingreso (salario mínimo diario)?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb24" >Menos de 6000</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb24" >Entre 6000 y 11000</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb24" >Entre 11000 y 20000</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb24" >Mas de 20000</label>
            </div>
           </div>
    </div>
           
                         <!-- pregunta 25 -->
       <div class="form-group">
        <label class="control-label">III.9 ¿Nivel jerárquico en el trabajo?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb25" >Tecnico</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb25" >Supervisor</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb25" >Jefe de Area</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb25" >Directivo</label>
            </div>
                                    <div class="radio ">
              <label><input type="radio" name="rb25" >Empresario</label>
            </div>
                                    <div class="radio ">
              <label><input type="radio" name="rb25" >Funcionario</label>
            </div>
           </div>
    </div>
                            <!-- pregunta 26 -->
       <div class="form-group">
        <label class="control-label">III.10 ¿Condición de Trabajo?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb26" >Base</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb26" >Eventual</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb26" >Contrato</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb26" >Otro</label>
            </div>
           </div>
    </div>
                             <!-- pregunta 27 -->
       <div class="form-group">
        <label class="control-label">III.11 ¿Relación del trabajo con su área de Formación Profesional?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb27" >0%</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb27" >20%</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb27" >40%</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb27" >60%</label>
            </div>
                            <div class="radio ">
              <label><input type="radio" name="rb27" >80%</label>
            </div>
                              <div class="radio ">
              <label><input type="radio" name="rb27" >100%</label>
            </div>
           </div>
    </div>
                                    <!-- pregunta 28 -->
       <div class="form-group">
        <label class="control-label">III.12 ¿Datos de la empresa u organismo?</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb28" >Publico</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb28" >Privado</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb28" >Social</label>
            </div>
           </div>
    </div>
        <!-- pregunta 29 -->
        <div class="form-group">
      <label class="control-label" for="electro_giroact">¿Giro o actividad principal de la empresa u Organismo?</label>
        <input type="text" class="form-control"  name="electro_giroact" required>
    </div>
                <!-- pregunta 30 -->
        <div class="form-group">
      <label class="control-label" for="electro_razonsocial">Razon Social:</label>
        <input type="text" class="form-control"  name="electro_razonsocial" required>
    </div>
        
            <!-- pregunta 31 -->
                  <div class="form-group">
            <div class="form-group">
            <label class="" for="electro_domiciloemp">Domicilio: </label>
                <input type="text" class="form-control" id="electro_domiciloemp" name="electro_domiciloemp">
                </div>
          <div class="form-group">
            <label class=" " for="electro_calleemp">Calle:</label>
            <input type="text" class="form-control" id="electro_calleemp"  name="electro_calleemp" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_numeroemp">No.#:</label>
            <input type="text" class="form-control" id="electro_numeroemp"  name="electro_numeroemp" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_coloniaemp">Colonia:</label>
            <input type="text" class="form-control" id="electro_coloniaemp"  name="electro_coloniaemp" required>
          </div>
              <div class="form-group">
            <label class=" " for="electro_cpemp">Codigo Postal:</label>
            <input type="text" class="form-control" id="electro_cpemp"  name="electro_cpemp" required>
          </div>
          </div>
        <!-- pregunta 32 -->
            <div class="form-group">
      <label class="control-label" for="electro_ciudademp">Ciudad:</label>
        <input type="text" class="form-control"  name="electro_ciudademp" required>
    </div>
                <!-- pregunta 32 -->
            <div class="form-group">
      <label class="control-label" for="electro_municipioemp">Municipio:</label>
        <input type="text" class="form-control"  name="electro_municipioemp" required>
    </div>
                        <!-- pregunta 33 -->
            <div class="form-group">
      <label class="control-label" for="electro_estadoemp">Estado:</label>
        <input type="text" class="form-control"  name="electro_estadoemp" required>
    </div>
                        <!-- pregunta 34 -->
            <div class="form-group">
      <label class="control-label" for="electro_telefonos">Telefonos:(01 -)</label>
        <input type="text" class="form-control"  name="electro_telefonos" required>
    </div>
                        <!-- pregunta 35 -->
            <div class="form-group">
      <label class="control-label" for="electro_telmix">Tel y Ext. Fax E-mail:</label>
        <input type="text" class="form-control"  name="electro_telmix" required>
    </div>
                         <!-- pregunta 36 -->
            <div class="form-group">
      <label class="control-label" for="electro_webemp">Pagina Web:</label>
        <input type="text" class="form-control"  name="electro_webemp" required>
    </div>
                                 <!-- pregunta 37 -->
            <div class="form-group">
      <label class="control-label" for="electro_nombrejefe">Nombre y puesto del Jefe Inmediato:</label>
        <input type="text" class="form-control"  name="electro_nombrejefe" required>
    </div>
                                            <!-- pregunta 38 -->
       <div class="form-group">
        <label class="control-label">III.13 Sector Económico de la Empresa u Organización</label>
           <label>Sector Primario</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb38-1" >Argoindustria</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-1" >Pesquero</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-1" >Otros</label>
            </div>
           </div>
           
                     <label>Sector Secundario</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb38-2" >Industrial</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-2" >Construccion</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-2" >Otros</label>
            </div>
           </div>
           
           
                     <label>Sector Terciario</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb38-3" >Hotelero</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-3" >Restaurante</label>
            </div>
                 <div class="radio ">
              <label><input type="radio" name="rb38-3" >Peritajes</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-3" >Otros</label>
            </div>
           </div>
    </div>
        
                    <!-- pregunta 39 -->
       <div class="form-group">
        <label class="control-label">III.14 Tamaño de la empresa u organización:</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb38-1" >MicroEmpresa (10-30)</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-1" >Pequeña(31-100)</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb38-1" >Mediana (101-500)</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="rb38-1" >Grande(mas de 500)</label>
            </div>
           </div>
        </div>
        
        <h2>IV. DESEMPEÑO PROFESIONAL (COHERENCIA ENTRE LA FORMACIÓN Y EL TIPO DE EMPLEO)</h2>
        <h4>Marcar los campos que correspondan a su trayectoria personal</h4>
        
        
                            <!-- pregunta 40 -->
       <div class="form-group">
        <label class="control-label">IV.1 Eficiencia para realizar las actividades laborales, en relación con su formación profesional</label>
        <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb40" >Muy Eficiente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb40" >Eficiente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb40" >Poco Eficiente</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="rb40" >Deficiente</label>
            </div>
           </div>
        </div>
                                <!-- pregunta 41 -->
       <div class="form-group">
        <label class="control-label">IV.2 ¿Cómo califica su formación profesional con respecto a su desempeño laboral?</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb41" >Excelente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb41" >Bueno</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb41" >Regular</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="rb41" >Malo</label>
            </div>
                    <div class="radio ">
              <label><input type="radio" name="rb41" >Pesimo</label>
            </div>
           </div>
        </div>
        
                                       <!-- pregunta 42 -->
       <div class="form-group">
        <label class="control-label">IV.3 Utilidad de las prácticas profesionales para su desarrollo laboral y profesional</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb42" >Excelente</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb42" >Bueno</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb42" >Regular</label>
            </div>
                       <div class="radio ">
              <label><input type="radio" name="rb42" >Malo</label>
            </div>
                    <div class="radio ">
              <label><input type="radio" name="rb42" >Pesimo</label>
            </div>
           </div>
        </div>
        
                                 <!-- pregunta 43 -->
       <div class="form-group">
        <label class="control-label">IV.4 Aspectos que valora la empresa u organismo para la contratación de profesionistas: (1 Poco, 5 Mucho)</label>
     <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1. Habilidad para crear y administrar cambios</td>
        <td><label ><input type="radio" name="rb43-1" required></label></td>
        <td><label ><input type="radio" name="rb43-1"></label></td>
        <td><label ><input type="radio" name="rb43-1"></label></td>
        <td><label ><input type="radio" name="rb43-1"></label></td>
        <td><label ><input type="radio" name="rb43-1"></label></td>
      </tr>
              <tr>
        <td>2. Alinear la tecnologia con la organizacion</td>
        <td><label ><input type="radio" name="rb43-2" required></label></td>
        <td><label ><input type="radio" name="rb43-2"></label></td>
        <td><label ><input type="radio" name="rb43-2"></label></td>
        <td><label ><input type="radio" name="rb43-2"></label></td>
        <td><label ><input type="radio" name="rb43-2"></label></td>
      </tr>
              <tr>
        <td>3. Experiencia Laboral</td>
        <td><label ><input type="radio" name="rb43-3" required></label></td>
        <td><label ><input type="radio" name="rb43-3"></label></td>
        <td><label ><input type="radio" name="rb43-3"></label></td>
        <td><label ><input type="radio" name="rb43-3"></label></td>
        <td><label ><input type="radio" name="rb43-3"></label></td>
      </tr>
              <tr>
        <td>4. Competencia Laboral</td>
        <td><label ><input type="radio" name="rb43-4" required></label></td>
        <td><label ><input type="radio" name="rb43-4"></label></td>
        <td><label ><input type="radio" name="rb43-4"></label></td>
        <td><label ><input type="radio" name="rb43-4"></label></td>
        <td><label ><input type="radio" name="rb43-4"></label></td>
      </tr>
              <tr>
        <td>5. Posicionamiento de la Institucion de Egreso</td>
        <td><label ><input type="radio" name="rb43-5" required></label></td>
        <td><label ><input type="radio" name="rb43-5"></label></td>
        <td><label ><input type="radio" name="rb43-5"></label></td>
        <td><label ><input type="radio" name="rb43-5"></label></td>
        <td><label ><input type="radio" name="rb43-5"></label></td>
      </tr>
              <tr>
        <td>6. Conocimiento de Idiomas Extranjeros</td>
        <td><label ><input type="radio" name="rb43-6" required></label></td>
        <td><label ><input type="radio" name="rb43-6"></label></td>
        <td><label ><input type="radio" name="rb43-6"></label></td>
        <td><label ><input type="radio" name="rb43-6"></label></td>
        <td><label ><input type="radio" name="rb43-6"></label></td>
      </tr>
              <tr>
        <td>7. Recomendaciones/referencia</td>
        <td><label ><input type="radio" name="rb43-7" required></label></td>
        <td><label ><input type="radio" name="rb43-7"></label></td>
        <td><label ><input type="radio" name="rb43-7"></label></td>
        <td><label ><input type="radio" name="rb43-7"></label></td>
        <td><label ><input type="radio" name="rb43-7"></label></td>
      </tr>
              <tr>
        <td>8. Personalidad/Actitudes</td>
        <td><label ><input type="radio" name="rb43-8" required></label></td>
        <td><label ><input type="radio" name="rb43-8"></label></td>
        <td><label ><input type="radio" name="rb43-8"></label></td>
        <td><label ><input type="radio" name="rb43-8"></label></td>
        <td><label ><input type="radio" name="rb43-8"></label></td>
      </tr>
              <tr>
        <td>9. Capacidad de liderazgo</td>
        <td><label ><input type="radio" name="rb43-9" required></label></td>
        <td><label ><input type="radio" name="rb43-9"></label></td>
        <td><label ><input type="radio" name="rb43-9"></label></td>
        <td><label ><input type="radio" name="rb43-9"></label></td>
        <td><label ><input type="radio" name="rb43-9"></label></td>
      </tr>
              <tr>
        <td>10. Transferencia de Conocimiento y tecnologia al campo laboral</td>
        <td><label ><input type="radio" name="rb43-10" required></label></td>
        <td><label ><input type="radio" name="rb43-10"></label></td>
        <td><label ><input type="radio" name="rb43-10"></label></td>
        <td><label ><input type="radio" name="rb43-10"></label></td>
        <td><label ><input type="radio" name="rb43-10"></label></td>
      </tr>
    </tbody>
  </table>
</setfield>
        </div>
                    <!-- pregunta 44 -->
         <h2>V. EXPECTATIVAS DE DESARROLLO, SUPERACIÓN PROFESIONAL Y DE ACTUALIZACIÓN:</h2>
        <h4>V.1 ACTUALIZACION DE CONOCIMIENTOS</h4>
       <div class="form-group">
        <label class="control-label">¿Le gustaría tomar cursos de actualización en Electromecánica?</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb44" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb44" >No</label>
            </div>
           </div>
        </div>
        
         <!-- pregunta 45 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_upd">¿Cuál?</label>
            <input type="text" class="form-control" id="electro_upd"  name="electro_upd" required>
          </div>
          </div>
        <!-- pregunta 46 -->
               <div class="form-group">
        <label class="control-label">¿Le gustaría tomar un Posgrado en Ciencias en Ingeniería Industrial?</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb46" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb46" >No</label>
            </div>
           </div>
        </div>
                <!-- pregunta 47 -->
               <div class="form-group">
        <label class="control-label">¿Le gustaría capacitarse en ingeniería mecatrónica?</label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb47" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb47" >No</label>
            </div>
           </div>
        </div>
                 <!-- pregunta 48 -->
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_part">VI. PARTICIPACIÓN SOCIAL DEL PROFESIONISTA:</label>
            <input type="text" class="form-control" id="electro_part"  name="electro_part" placeholder="(Opcional)">
          </div>
          </div>
                        <!-- pregunta 49 -->
               <div class="form-group">
        <label class="control-label">VI.1 ¿Pertenece a organizaciones sociales:? </label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb49" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb49" >No</label>
            </div>
           </div>
        </div>
                 <!-- pregunta 50 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_soc">¿Cuáles?</label>
            <input type="text" class="form-control" id="electro_soc"  name="electro_soc" required>
          </div>
          </div>
                               <!-- pregunta 51 -->
               <div class="form-group">
        <label class="control-label">VI.2 ¿Pertenece a organizaciones profesionistas:? </label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb49" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb49" >No</label>
            </div>
           </div>
        </div>
                         <!-- pregunta 52 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_profesionales">¿Cuáles?</label>
            <input type="text" class="form-control" id="electro_profesionales"  name="electro_profesionales" required>
          </div>
          </div>
                   <!-- pregunta 53 -->
               <div class="form-group">
        <label class="control-label">VI.3 ¿Pertenece a la asociación de egresados? </label>
         <div class="radio">
            <div class="radio ">
              <label><input type="radio" name="rb49" >Si</label>
            </div>
                        <div class="radio ">
              <label><input type="radio" name="rb49" >No</label>
            </div>
           </div>
        </div>
                          <!-- pregunta 52 -->
        
                  <div class="form-group">
          <div class="form-group">
            <label class="control-label col-sm-10 " for="electro_egresados">¿Cuál?</label>
            <input type="text" class="form-control" id="electro_egresados"  name="electro_egresados" required>
          </div>
          </div>
        

<!-- ###################################################### -->

<div class="form-group">
      <label class="control-label" for="nombre">COMENTARIOS Y SUGERENCIAS:</label>
        <textarea class="form-control" id="det" placeholder="Quejas o comentarios" name="det"></textarea>
 </div>
<div class="form-group">
      <label class="control-label" for="nombre">Opinión o recomendación para mejorar la formación profesional de un profesionista de su carreraOpinión o recomendación para mejorar la formación profesional de un profesionista de su carrera</label>
        <textarea class="form-control" id="opiniones" placeholder="Opiniones" name="opiniones"></textarea>
 </div>


  <div class="form-group">
   <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
  </div>
</form>
</div>

</div>
<footer class="row">
      <div class="col-md-12">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </footer>

<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>
 <!-- bootbox code -->
    <script src="../js/bootbox.min.js"></script>
    <script>
        $(document).on("click", ".alert", function(e) {
          bootbox.alert({
    message:"Por favor lea cuidadosamente y conteste este cuestionario de la siguiente manera, según sea el caso:<br><br>1. En el caso de preguntas cerradas, marque la que considere aprrb4iada de esta manera:(x) <br><br>2. En las preguntas de valoración se utiliza la escala del 1 al 5 en la que 1 es “muy malo” y 5 es “muy bueno”.<br><br>3. En los casos de preguntas abiertas dejamos un espacio para que usted escriba con mayúscula una respuesta (______________________________).<br>Si el espacio para su respuesta no es suficiente, por favor añada una hoja adicional al cuestionario.<br><br>4. Al final anexamos un inciso para comentarios y sugerencias, le agradeceremos anote ahí lo que considere prudente para mejorar nuestro sistema educativo o bien los temas que, a su juicio, omitimos en el cuestionario.<br><br>Gracias por su gentil colaboración.",
    backdrop: true,
    size: 'large'
});
        });
    </script>
</body>
</html>
