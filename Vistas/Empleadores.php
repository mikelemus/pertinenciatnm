<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="UTF-8">


	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="CSS/Footer.css">
    <link rel="stylesheet" href="CSS/style.css">

	<!-- <link rel="stylesheet" href="css/bootstrap-them.min.css">

    https://www.w3schools.com/tags/att_input_.asp
    https://www.w3schools.com/html/html_form_input_types.asp
    https://www.w3schools.com/bootstrap/bootstrap_get_started.asp
    https://docs.google.com/forms/d/e/1FAIpQLScsNGOXW2c9SlNSjDNLnB6O-Mv-qWT-Lkq1wkU1KCfcMIVfZQ/viewform

	 -->
	<title>Programa de Pertinencia</title>
</head>
<body>
	<div class="container">
  	<header class="row">
			<div class="col-md-12">
      	<img src="imagen/HeaderCIITNE.jpg" class="img-responsive" alt="Imagen responsive" height="160" width="1140">
      </div>
		</header>
    <?php require 'nav.php';?>
    <div class="jumbotron">
      <div class="container text-danger text-center">
        <h2>CUESTIONARIO PARA EMPLEADORES </h2>
      </div>
      <div class="container text-center"> <h3>Sector Productivo y de Servicios</h3></div>
      <div class="container text-center">
        <p>INSTITUTO TECNOLÓGICO SUPERIOR JOSÉ MARIO MOLINA PASQUEL Y HENRÍQUEZ UNIDAD ACADÉMICA PUERTO VALLARTA, JALISCO</p>
      </div>
    </div>
            <form class="" action="../Controles/conInsertarEmpleador.php" method="POST">
      <div class="jumbotron">
        <div class="form-group">
          <label class="col-sm-12 text-center" ><p>Introduce tu email</p></label>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" placeholder="email@dominio.com" name="orgCorreo" required>
        </div>
      </div>
      <div class="jumbotron col-sm-12">
        <div class="container text-justify col-sm-8 col-sm-offset-2">
      <button type="button" class="btn btn-danger btn-lg btn-block alert"><H5><label class="glyphicon glyphicon-align-left  glyphicon-info-sign"></label> INSTRUCIONES</H5></button>

        </div>
      </div>

<div class="jumbotron col-sm-12">

  <h2>A. DATOS GENERALES DE LA EMPRESA U ORGANISMO</h2>

  <p>Empleador</p>



    <div class="form-group">
      <label class=" col-sm-10" for="Empresa_domicilo">1.Nombre Comercial de la Empresa:</label>
      <input type="text" class="form-control" id="Empresa_nombre" placeholder="Nombre de la  Empresa" name="orgNombre" required>
    </div>

    <div class="form-group">
      <label class=" col-sm-10" for="Empresa_domicilo">Domicilio:</label>
      <input type="text" class="form-control" id="Empresa_domicilo" placeholder="Direccion Completa" name="orgDomicilio" required>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_ciudad">Ciudad:</label>

        <input type="text" class="form-control" id="Empresa_ciudad" placeholder="Ciudad" name="orgCiudad" required>

    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_municipio">Municipio:</label>

        <input type="text" class="form-control" id="Empresa_municipio" placeholder="Municipio" name="orgMunicipio" required>

    </div>


     <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_estado">Estado:</label>

        <input type="rfc" class="form-control" id="Empresa_estado" placeholder="Estado" name="orgEstado" required>

    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_tel">Teléfono:</label>

        <input type="number" class="form-control" id="Empresa_tel" placeholder="Numero a 10 digitos" name="orgTelefono" required>

    </div>

    <!--<div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_mail">Email de la Empresa:</label>
      <input type="email" class="form-control" id="Empresa_mail" placeholder="email@dominio.com" name="Empresa_mail" disabled="true">
    </div>
  -->


    <div class="form-group">
      <label class="control-label">2.Su empresa u organismo es:</label>
      <div class="radio">
      <label><input type="radio" name="orgSector" value="publica" required>Pública</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgSector" value="privada" required>Privada</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgSector" value="social" required>Social</label>
    </div>
</div>


<div class="form-group">
      <label class="control-label" for="Empresa_tipo">3.-Tamaño de la empresa u organismo:</label>
     <div class="radio">
      <label><input type="radio" name="orgTamano" value="micro" required>Microempresa (1 -30)</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgTamano" value="pequena" required>Pequeña (31-100)</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgTamano" value="mediana" required>Mediana (101-500)</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgTamano" value="grande" required>  Grande (más de 500)</label>
    </div>

  </div>

   <div class="form-group">
      <label class="control-label" for="Empresa_tipo">4.-Actividad de su empresa u organismo</label>
     <div class="radio">
      <label><input type="radio" name="orgActividad" value="pya" required>Pesca y Acuicultura</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgActividad" value="ayb" required>Alimentos y Bebidas</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgActividad" value="tvyc" required>Textiles, Vestido y Cuero</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgActividad" value="myp" required>Madera y sus productos</label>
    </div>

     <div class="radio ">
      <label><input type="radio" name="orgActividad" value="piye" required>Papel, Imprenta y editoriales</label>
    </div>

     <div class="radio ">
      <label><input type="radio" name="orgActividad" value="quim" required>Química</label>
    </div>

    <div class="radio ">
      <label><input type="radio" name="orgActividad" value="otros" required>Otros</label>
    </div>
  </div>

    <div class="form-group">
      <label class="control-label" for="Empresa_tipo">5.- Indique a cuál clasificación corresponde su empresa</label>
     <div class="radio">
      <label><input type="radio" name="orgClasificacion" value="mineralesnometalicos" required>Minerales no metálicos</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgClasificacion" value="industriasmetalicasbasicas" required>Industrias metálicas básicas</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgClasificacion" value="productosmetalicosmaquinariasyequipos" required>Productos metálicos, maquinaria y equipo</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" value="construccion" required>Construcción</label>
    </div>
     <div class="radio ">
      <label><input type="radio" name="orgClasificacion" value="electricidadgasyagua" required>Electricidad, gas y agua</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" value="comercialyturismo" required>Comercial y turismo</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" value="transportealmacenajeycomunicaicones" required>Transporte, almacenaje y comunicaciones/label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" value="serviciosfinancierosseguros" required>Servicios financieros, seguros, actividades inmobiliarias y de alquiler</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" value="educacion" required>Educación</label>
    </div>
    <div class="input-group">
      <span class="input-group-addon">
        <input type="radio" name ="orgClasificacion" aria-label="..." required>
        Otro
      </span>
        <input type="text" class="form-control" aria-label="..." name="orgClasificacion" required>
    </div><!-- /input-group -->
  </div>
  <h2>B. UBICACIÓN LABORAL DE LOS EGRESADOS</h2>

    <div class="form-group">
      <label class="control-label">6.-número de profesionistas con nivel de licenciatura que laboran en la empresa u organismo</label>
      <div class="radio">
      <label><input type="radio" name="egrNumProfesionistas" value="1" required>1</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="egrNumProfesionistas" value="2-5" required>de 2 a 5</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="egrNumProfesionistas" value="6-8" required>de 6 a 8</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="egrNumProfesionistas" value="9-10" required>de 9-a 10</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="egrNumProfesionistas" value=">10" required>más de 10</label>
    </div>
</div>

<div class="form-group">
      <label class="control-label">7.- Indique el nivel jerárquico que ocupan los alumnos del Instituto Tecnológico es su organización<br></label>
<div class="table-responsive">
<setfield >
<table class=" col-sm-12 table table-striped table-hover" id="tabla">
    <thead>
      <tr>
        <th></th>
        <th>Mando superior</th>
        <th>Mando intermedio</th>
        <th>Supervisor o equivalente</th>
        <th>Técnico o auxiliar</th>
        <th>Ninguno</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Licenciatura en Arquitectura</td>
        <td><label ><input type="radio" name="egrNivelArq" value="mandosuperior" required></label></td>
        <td><label ><input type="radio" name="egrNivelArq" value="mandointermedio" required></label></td>
        <td><label ><input type="radio" name="egrNivelArq" value="supervoequivalente" required></label></td>
        <td><label ><input type="radio" name="egrNivelArq" value="tecnicoauxiliar" required></label></td>
        <td><label ><input type="radio" name="egrNivelArq" value="ninguno" required></label></td>
      </tr>
      <tr>
        <td>Licenciatura en Turismo</td>
        <td><label><input type="radio" name="egrNivelTur" value="mandosuperior" required></label></td>
        <td><label><input type="radio" name="egrNivelTur" value="mandointermedio" required></label></td>
        <td><label><input type="radio" name="egrNivelTur" value="supervoequivalente" required></label></td>
        <td><label><input type="radio" name="egrNivelTur" value="tecnicoauxiliar" required></label></td>
        <td><label><input type="radio" name="egrNivelTur" value="ninguno" required></label></td>
      </tr>
      <tr>
        <td>Licenciatura en Gastronomia</td>
        <td><label><input type="radio" name="egrNivelGas" value="mandosuperior" required></label></td>
        <td><label><input type="radio" name="egrNivelGas" value="mandointermedio" required></label></td>
        <td><label><input type="radio" name="egrNivelGas" value="supervoequivalente" required></label></td>
        <td><label><input type="radio" name="egrNivelGas" value="tecnicoauxiliar" required></label></td>
        <td><label><input type="radio" name="egrNivelGas" value="ninguno" required></label></td>
      </tr>
     
      <tr>
       <td>Ingenieria en Tecnologías de información</td>
       <td><label ><input type="radio" name="egrNivelTic" value="mandosuperior" required></label></td>
       <td><label ><input type="radio" name="egrNivelTic" value="mandointermedio" required></label></td>
       <td><label ><input type="radio" name="egrNivelTic" value="supervoequivalente" required></label></td>
       <td><label ><input type="radio" name="egrNivelTic" value="tecnicoauxiliar" required></label></td>
       <td><label ><input type="radio" name="egrNivelTic" value="ninguno" required></label></td>
      </tr>
      <tr>
       <td>Ingenieria Electromecánica</td>
       <td><label ><input type="radio" name="egrNivelEle" value="mandosuperior" required></label></td>
       <td><label ><input type="radio" name="egrNivelEle" value="mandointermedio" required></label></td>
       <td><label ><input type="radio" name="egrNivelEle" value="supervoequivalente" required></label></td>
       <td><label ><input type="radio" name="egrNivelEle" value="tecnicoauxiliar" required></label></td>
       <td><label ><input type="radio" name="egrNivelEle" value="ninguno" required></label></td>
      </tr>
      <tr>
        <td>Ingenieria En Gestion Empresarial</td>
        <td><label><input type="radio" name="egrNivelIge" value="mandosuperior" required></label></td>
        <td><label><input type="radio" name="egrNivelIge" value="mandointermedio" required></label></td>
        <td><label><input type="radio" name="egrNivelIge" value="supervoequivalente" required></label></td>
        <td><label><input type="radio" name="egrNivelIge" value="tecnicoauxiliar" required></label></td>
        <td><label><input type="radio" name="egrNivelIge" value="ninguno" required></label></td>
      </tr>
      <tr>
        <td>Ingenieria en sistemas Computacionales</td>
        <td><label><input type="radio" name="egrNivelSis" value="mandosuperior" required></label></td>
        <td><label><input type="radio" name="egrNivelSis" value="mandointermedio" required></label></td>
        <td><label><input type="radio" name="egrNivelSis" value="supervoequivalente" required></label></td>
        <td><label><input type="radio" name="egrNivelSis" value="tecnicoauxiliar" required></label></td>
        <td><label><input type="radio" name="egrNivelSis" value="ninguno" required></label></td>
      </tr>
    </tbody>
  </table>
</setfield>
</div>
</div>

<div class="form-group">
      <label class="control-label">8.- Indique el perfil de egreso que gustaria<br></label>
<div class="table-responsive">
<setfield >
<table class=" col-sm-12 table table-striped table-hover" id="tabla">
    <thead>
      <tr>
        <th></th>
        <th>Perfil De Egreso</th>
       </tr>
    </thead>
    <tbody>
      <tr>
        <td>Licenciatura en Arquitectura</td>
        <td><label ><input type="text" name="egrPerArq" required></label></td>
        </tr>
      <tr>
        <td>Licenciatura en  Turismo</td>
        <td><label><input type="text" name="egrPerTur" required></label></td>
       
      </tr>
      <tr>
        <td>Licenciatura en  Gastronomia</td>
        <td><label><input type="text" name="egrPerGas" required></label></td>
        
      </tr>
     
      <tr>
       <td>Ingenieria en Tecnologías de información</td>
       <td><label ><input type="text" name="egrPerTic" required></label></td>
       
      </tr>
      <tr>
       <td>Ingenieria en Electromecánica</td>
       <td><label ><input type="text" name="egrPerEle" required></label></td>
       
      </tr>
      <tr>
        <td>Ingenieria en Gestion Empresarial</td>
        <td><label><input type="text" name="egrPerIge" required></label></td>
      
      </tr>
      <tr>
        <td>Ingenieria en  Sistemas Computacionales</td>
        <td><label><input type="text" name="egrPerSis" required></label></td>
     
      </tr>
    </tbody>
  </table>
</setfield>
</div>
</div>




<div class="form-group">
      <label class="control-label">9.- Congruencia entre el perfil profesional que desarrollan los egresados del Instituto Tecnológico en su empresa u organización. Del total de sus egresados anote el porcentaje corresponde</label>
     <div class="table-responsive">
     <table id="tabla" class=" col-sm-12 table table-striped table-hover">
    <thead>
      <tr>
        <th></th>
        <th>Completamente</th>
        <th>Medianamente</th>
        <th>Ligeramente</th>
        <th>Ninguna Relacion</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Licenciatura en Arquitectura</td>
        <td><label ><input type="radio" name="cgrNivelArq" value="completamente" required></label></td>
        <td><label ><input type="radio" name="cgrNivelArq" value="medianamente" required></label></td>
        <td><label ><input type="radio" name="cgrNivelArq" value="ligeramente" required></label></td>
        <td><label ><input type="radio" name="cgrNivelArq" value="ningunarelacion" required></label></td>
        
      </tr>
      <tr>
        <td>Licenciatura en Turismo</td>
        <td><label><input type="radio" name="cgrNivelTur" value="completamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelTur" value="medianamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelTur" value="ligeramente" required></label></td>
        <td><label><input type="radio" name="cgrNivelTur" value="ningunarelacion" required></label></td>
        
      </tr>
      <tr>
        <td>Licenciatura en Gastronomia</td>
        <td><label><input type="radio" name="cgrNivelGas" value="completamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelGas" value="medianamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelGas" value="ligeramente" required></label></td>
        <td><label><input type="radio" name="cgrNivelGas" value="ningunarelacion" required></label></td>
        
      </tr>
     <tr>
       <td>Ingenieria en Tecnologías de información</td>
       <td><label ><input type="radio" name="cgrNivelTic" value="completamente" required></label></td>
       <td><label ><input type="radio" name="cgrNivelTic" value="medianamente" required></label></td>
       <td><label ><input type="radio" name="cgrNivelTic" value="ligeramente" required></label></td>
       <td><label ><input type="radio" name="cgrNivelTic" value="ningunarelacion" required></label></td>
       
      </tr>
      <tr>
       <td>Ingenieria en Electromecánica</td>
       <td><label ><input type="radio" name="cgrNivelEle" value="completamente" required></label></td>
       <td><label ><input type="radio" name="cgrNivelEle" value="medianamente" required></label></td>
       <td><label ><input type="radio" name="cgrNivelEle" value="ligeramente" required></label></td>
       <td><label ><input type="radio" name="cgrNivelEle" value="ningunarelacion" required></label></td>
       
      </tr>
      <tr>
        <td>Ingenieria en Gestion Empresarial</td>
        <td><label><input type="radio" name="cgrNivelIge" value="completamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelIge" value="medianamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelIge" value="ligeramente" required></label></td>
        <td><label><input type="radio" name="cgrNivelIge" value="ningunarelacion" required></label></td>
        
      </tr>
      <tr>
        <td>Ingenieria en  Sis. Computacionales</td>
        <td><label><input type="radio" name="cgrNivelSis" value="completamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelSis" value="medianamente" required></label></td>
        <td><label><input type="radio" name="cgrNivelSis" value="ligeramente" required></label></td>
        <td><label><input type="radio" name="cgrNivelSis" value="ningunarelacion" required></label></td>
        
      </tr>
    
    </tbody>
  </table>
</div>
</div>
<div class="form-group">
      <label class="control-label">10.- Requisitos que establece su empresa u organización para la contratación de personal con nivel licenciatura</label>
      <div class="radio">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Área o campo de estudio-" required>Área o campo de estudio</label>
    </div>
    <div class="radio">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Titulación-" required>Titulación</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Experiencia laboral/ práctica-" required>Experiencia laboral/ práctica( antes de egresar)</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Posicionamiento de la institución de egresos-" required>Posicionamiento de la institución de egresos</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Conocimientos de idiomas extranjeros-" required>Conocimientos de idiomas extranjeros</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Recomendaciones/ Referencias-" required>Recomendaciones/ Referencias</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Personalidad/ actitudes-" required>Personalidad/ actitudes</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Capacidad de liderazgo-" required>Capacidad de liderazgo</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresa[]" value="Otro-" required>Otro</label>
    </div>
</div>
<!-- pregunta 9 -->


 <!-- pregunta 10 -->
<div class="form-group">
      <label class=" control-label">11. Carreras que demanda preferentemente su Empresa u organismo:</label>
     <div class="radio">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Arquitectura-" required>Arquitectura</label>
    </div>
    <div class="radio">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Turismo-" required>Turismo</label>
    </div>
    <div class="radio">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Gastronomía-" required>Gastronomía</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Ingeniería en tecnologías de la información y comunicaciones-" required> Ingeniería en tecnologías de la información y comunicaciones</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Ingeniería en electromecánica-" required>Ingeniería en electromecánica</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Ingeniería en gestión empresarial-" required>Ingeniería en gestión empresarial</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Ingeniería en sistemas computacionales-" required>Ingeniería en sistemas computacionales</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemanda[]" value="Otro-" required>Otro: </label>
    </div>
  </div>

     <!-- pregunta 11 -->
<div class="form-group">
      <label class="control-label"><h4>12. ¿Cómo considera las especialidades de las carreras que se ofertan en el Instituto Tecnológico Superior?</h4></label>

    <label class="control-label">Desarrollo de negocios e innovación tecnológica (Ingenieria en  En gestion empresarial):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspNegocios" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspNegocios" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspNegocios" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspNegocios" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspNegocios" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Aplicaciones para dispositivos móviles (Ingenieria en En sistemas computacionales):</label>
    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspMoviles" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspMoviles" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspMoviles" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspMoviles" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspMoviles" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Desarrollo sustentable y urbanismo (Arquitectura):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspSustentable" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspSustentable" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspSustentable" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspSustentable" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspSustentable" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Redes y comunicación de datos (Ingenieria en  En tecnologías de la información y la comunicación):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspRedes" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspRedes" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspRedes" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspRedes" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspRedes" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Automatización y electrificación (Ingenieria en  Electromecánica):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspAutomatizacion" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Administración de tendencias culinarias de vanguardia (Licenciatura en  Gastronomía):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspCulinarias" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

<label class="col-sm-12">(Turismo):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspTurism" value="1" required></label></td>
        <td><label ><input type="radio" name="egrEspTurism" value="2" required></label></td>
        <td><label ><input type="radio" name="egrEspTurism" value="3" required></label></td>
        <td><label ><input type="radio" name="egrEspTurism" value="4" required></label></td>
        <td><label ><input type="radio" name="egrEspTurism" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

    </div>

     <!-- pregunta 12 -->
<div class="form-group">
    <label class="control-label" for="respuesta12">13. ¿Qué especialidad sugeriría? </label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="egrEspecialidadSugerida" >

  </div>

 <!-- pregunta 13 -->
<div class="form-group">
      <label class="control-label"><h4>14. ¿Cómo considera las líneas de trabajo de la maestría en Administración que se oferta en el Tec Vallarta?</h4></label>

    <label class="col-sm-12">Innovación, Productividad y Tecnología Para la Competitividad Internacional (En administración):</label>

        <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrLineaInnovacion" value="1" required></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion" value="2" required></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion" value="3" required></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion" value="4" required></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

    <label class="col-sm-12">Estrategias para la alta dirección (En administración):</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrLineaEstrategicas" value="1" required></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas" value="2" required></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas" value="3" required></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas" value="4" required></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas" value="5" required></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

    </div>

 <!-- pregunta 14 -->
<div class="form-group">
        <label class="control-label" for="respuesta12">15. ¿Qué maestria sugeriría usted?
</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="egrMaestriaSugerida" required >

  </div>

 <!-- pregunta 15 -->
<div class="form-group">
      <label class="control-label"><h2>C. COMPETENCIAS LABORALES</h2></label>

    <label class="control-label">16. En su opinión ¿qué competencias considera deben desarrollar los egresados del Tec Vallarta, para desempeñarse eficientemente en sus actividades laborales? Por favor evalúe conforme a las siguientes opciones:</label>

        <label class="col-sm-12">Habilidad para resolver conflictos:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comConflictos" value="1" required></label></td>
        <td><label ><input type="radio" name="comConflictos" value="2" required></label></td>
        <td><label ><input type="radio" name="comConflictos" value="3" required></label></td>
        <td><label ><input type="radio" name="comConflictos" value="4" required></label></td>
        <td><label ><input type="radio" name="comConflictos" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Ortografía y redacción de documentos:</label>

    <setfield >
<table class="table table-responsive table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comRedaccion" value="1" required></label></td>
        <td><label ><input type="radio" name="comRedaccion" value="2" required></label></td>
        <td><label ><input type="radio" name="comRedaccion" value="3" required></label></td>
        <td><label ><input type="radio" name="comRedaccion" value="4" required></label></td>
        <td><label ><input type="radio" name="comRedaccion" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Mejora de procesos:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comProcesos" value="1" required></label></td>
        <td><label ><input type="radio" name="comProcesos" value="2" required></label></td>
        <td><label ><input type="radio" name="comProcesos" value="3" required></label></td>
        <td><label ><input type="radio" name="comProcesos" value="4" required></label></td>
        <td><label ><input type="radio" name="comProcesos" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Trabajo en equipo:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comTrabajoE" value="1" required></label></td>
        <td><label ><input type="radio" name="comTrabajoE" value="2" required></label></td>
        <td><label ><input type="radio" name="comTrabajoE" value="3" required></label></td>
        <td><label ><input type="radio" name="comTrabajoE" value="4" required></label></td>
        <td><label ><input type="radio" name="comTrabajoE" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Seguridad personal:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comSeguridad" value="1" required></label></td>
        <td><label ><input type="radio" name="comSeguridad" value="2" required></label></td>
        <td><label ><input type="radio" name="comSeguridad" value="3" required></label></td>
        <td><label ><input type="radio" name="comSeguridad" value="4" required></label></td>
        <td><label ><input type="radio" name="comSeguridad" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Facilidad de palabra:</label>

        <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comFacilidad" value="1" required></label></td>
        <td><label ><input type="radio" name="comFacilidad" value="2" required></label></td>
        <td><label ><input type="radio" name="comFacilidad" value="3" required></label></td>
        <td><label ><input type="radio" name="comFacilidad" value="4" required></label></td>
        <td><label ><input type="radio" name="comFacilidad" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Gestión de Proyectos:</label>

        <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comGestion" value="1" required></label></td>
        <td><label ><input type="radio" name="comGestion" value="2" required></label></td>
        <td><label ><input type="radio" name="comGestion" value="3" required></label></td>
        <td><label ><input type="radio" name="comGestion" value="4" required></label></td>
        <td><label ><input type="radio" name="comGestion" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Puntualidad y Asistencia:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comPuntualidad" value="1" required></label></td>
        <td><label ><input type="radio" name="comPuntualidad" value="2" required></label></td>
        <td><label ><input type="radio" name="comPuntualidad" value="3" required></label></td>
        <td><label ><input type="radio" name="comPuntualidad" value="4" required></label></td>
        <td><label ><input type="radio" name="comPuntualidad" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Cumplimiento de las normas:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comCumplimiento" value="1" required></label></td>
        <td><label ><input type="radio" name="comCumplimiento" value="2" required></label></td>
        <td><label ><input type="radio" name="comCumplimiento" value="3" required></label></td>
        <td><label ><input type="radio" name="comCumplimiento" value="4" required></label></td>
        <td><label ><input type="radio" name="comCumplimiento" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Integración al trabajo:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comIntegracion" value="1" required></label></td>
        <td><label ><input type="radio" name="comIntegracion" value="2" required></label></td>
        <td><label ><input type="radio" name="comIntegracion" value="3" required></label></td>
        <td><label ><input type="radio" name="comIntegracion" value="4" required></label></td>
        <td><label ><input type="radio" name="comIntegracion" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Creatividad e innovación:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comCreatividad" value="1" required></label></td>
        <td><label ><input type="radio" name="comCreatividad" value="2" required></label></td>
        <td><label ><input type="radio" name="comCreatividad" value="3" required></label></td>
        <td><label ><input type="radio" name="comCreatividad" value="4" required></label></td>
        <td><label ><input type="radio" name="comCreatividad" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Capacidad de negociación:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comNegociacion" value="1" required></label></td>
        <td><label ><input type="radio" name="comNegociacion" value="2" required></label></td>
        <td><label ><input type="radio" name="comNegociacion" value="3" required></label></td>
        <td><label ><input type="radio" name="comNegociacion" value="4" required></label></td>
        <td><label ><input type="radio" name="comNegociacion" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Capacidad de abstracción, análisis y síntesis:</label>
    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comAbstraccion" value="1" required></label></td>
        <td><label ><input type="radio" name="comAbstraccion" value="2" required></label></td>
        <td><label ><input type="radio" name="comAbstraccion" value="3" required></label></td>
        <td><label ><input type="radio" name="comAbstraccion" value="4" required></label></td>
        <td><label ><input type="radio" name="comAbstraccion" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Liderazgo y toma de decisiones:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comLiderazgo" value="1" required></label></td>
        <td><label ><input type="radio" name="comLiderazgo" value="2" required></label></td>
        <td><label ><input type="radio" name="comLiderazgo" value="3" required></label></td>
        <td><label ><input type="radio" name="comLiderazgo" value="4" required></label></td>
        <td><label ><input type="radio" name="comLiderazgo" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Adaptación al cambio:</label>

    <setfield >
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comAdaptacion" value="1" required></label></td>
        <td><label ><input type="radio" name="comAdaptacion" value="2" required></label></td>
        <td><label ><input type="radio" name="comAdaptacion" value="3" required></label></td>
        <td><label ><input type="radio" name="comAdaptacion" value="4" required></label></td>
        <td><label ><input type="radio" name="comAdaptacion" value="5" required></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Otras (especifique):</label>

    </div>

<!-- pregunta 16 -->
<!--<div class="form-group">
        <label class="control-label">16. Con base al desempeño laboral así como a las actividades laborales que realiza el egresado ¿Cómo considera su desempeño laboral respecto a su formación académica? Del total de egresados anote el porcentaje que corresponda :</label>
    <div class="table-responsive">
    <setfield >
<table id="tabla" class="table table-striped ">
    <thead>
      <tr>
        <th></th>
        <th>20%</th>
        <th>40%</th>
        <th>60%</th>
        <th>80%</th>
        <th>100%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Excelente</td>
        <td><label ><input type="radio" name="comDesempenoE" ></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
      </tr>

      <tr>
        <td>Muy Bueno</td>
        <td><label ><input type="radio" name="comDesempenoMB" ></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
      </tr>

      <tr>
        <td>Bueno</td>
        <td><label ><input type="radio" name="comDesempenoB" ></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
      </tr>

      <tr>
        <td>Regular</td>
        <td><label ><input type="radio" name="comDesempenoR" ></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
      </tr>

      <tr>
        <td>Malo</td>
        <td><label ><input type="radio" name="comDesempenoM" ></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
      </tr>
    </tbody>
  </table>
</setfield>
    </div>
    </div>

  -->

<!-- pregunta 17 -->
<div class="form-group">
    <label class="control-label" for="respuesta17">18. De acuerdo con las necesidades de su empresa u organismo, ¿qué sugiere para mejorar la formación de los egresados Tec Vallarta?</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="comMejoraEgresado" required >


<div class="form-group">
      <label class="control-label col-sm-2" for="nombre">Comentarios:</label>
        <textarea class="form-control" id="det" placeholder="Quejas o comentarios" name="comComentario" required></textarea>
 </div>


  <div class="form-group">
   <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
  </div>
</div>
</form>
</div>
<footer class="row">
			<div class="col-md-12">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>
		</footer>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
 <!-- bootbox code -->
    <script src="js/bootbox.min.js"></script>
    <script>
        $(document).on("click", ".alert", function(e) {
          bootbox.alert({
    message:"Por favor lea cuidadosamente y conteste este cuestionario de la siguiente manera, según sea el caso:<br><br>1. En el caso de preguntas cerradas, marque la que considere aprrb4iada de esta manera:(x) <br><br>2. En las preguntas de valoración se utiliza la escala del 1 al 5 en la que 1 es “muy malo” y 5 es “muy bueno”.<br><br>3. En los casos de preguntas abiertas dejamos un espacio para que usted escriba con mayúscula una respuesta (______________________________).<br>Si el espacio para su respuesta no es suficiente, por favor añada una hoja adicional al cuestionario.<br><br>4. Al final anexamos un inciso para comentarios y sugerencias, le agradeceremos anote ahí lo que considere prudente para mejorar nuestro sistema educativo o bien los temas que, a su juicio, omitimos en el cuestionario.<br><br>Gracias por su gentil colaboración.",
    backdrop: true,
    size: 'large'
});
        });
    </script>
</body>
</html>
