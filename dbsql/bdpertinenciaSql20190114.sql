-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-01-2019 a las 20:34:42
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdpertinencia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcompetencias`
--

CREATE TABLE `tblcompetencias` (
  `comId` int(11) NOT NULL,
  `comCorreo` varchar(60) DEFAULT NULL,
  `comConflictos` varchar(2) DEFAULT NULL,
  `comRedaccion` varchar(2) DEFAULT NULL,
  `comProcesos` varchar(2) DEFAULT NULL,
  `comTrabajoE` varchar(2) DEFAULT NULL,
  `comSeguridad` varchar(2) DEFAULT NULL,
  `comFacilidad` varchar(2) DEFAULT NULL,
  `comGestion` varchar(2) DEFAULT NULL,
  `comPuntualidad` varchar(2) DEFAULT NULL,
  `comCumplimiento` varchar(2) DEFAULT NULL,
  `comIntegracion` varchar(2) DEFAULT NULL,
  `comCreatividad` varchar(2) DEFAULT NULL,
  `comNegociacion` varchar(2) DEFAULT NULL,
  `comAbstraccion` varchar(2) DEFAULT NULL,
  `comLiderazgo` varchar(2) DEFAULT NULL,
  `comAdaptacion` varchar(2) DEFAULT NULL,
  `comMejoraEgresado` varchar(300) DEFAULT NULL,
  `comComentario` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblcompetencias`
--

INSERT INTO `tblcompetencias` (`comId`, `comCorreo`, `comConflictos`, `comRedaccion`, `comProcesos`, `comTrabajoE`, `comSeguridad`, `comFacilidad`, `comGestion`, `comPuntualidad`, `comCumplimiento`, `comIntegracion`, `comCreatividad`, `comNegociacion`, `comAbstraccion`, `comLiderazgo`, `comAdaptacion`, `comMejoraEgresado`, `comComentario`) VALUES
(1, 'luis.lopez@vallarta.tecmm.edu.mx', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'dsdsd', 'sdsd'),
(2, 'lord_1@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'lord_1@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, '', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(5, '', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(6, 'lord_Palomino@gmail.com', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(7, 'lord_Palomino@gmail.com', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbldesempeno`
--

CREATE TABLE `tbldesempeno` (
  `desId` int(11) NOT NULL,
  `desCorreo` varchar(60) DEFAULT NULL,
  `desEficiencia` varchar(20) DEFAULT NULL,
  `desCalifica` varchar(20) DEFAULT NULL,
  `desUtilidad` varchar(20) DEFAULT NULL,
  `desAspecto1` int(11) DEFAULT NULL,
  `desAspecto2` int(11) DEFAULT NULL,
  `desAspecto3` int(11) DEFAULT NULL,
  `desAspecto4` int(11) DEFAULT NULL,
  `desAspecto5` int(11) DEFAULT NULL,
  `desAspecto6` int(11) DEFAULT NULL,
  `desAspecto7` int(11) DEFAULT NULL,
  `desAspecto8` int(11) DEFAULT NULL,
  `desAspecto9` int(11) DEFAULT NULL,
  `desAspecto10` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbldesempeno`
--

INSERT INTO `tbldesempeno` (`desId`, `desCorreo`, `desEficiencia`, `desCalifica`, `desUtilidad`, `desAspecto1`, `desAspecto2`, `desAspecto3`, `desAspecto4`, `desAspecto5`, `desAspecto6`, `desAspecto7`, `desAspecto8`, `desAspecto9`, `desAspecto10`) VALUES
(4, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Muy eficiente', 'Excelente', 'Excelente', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3),
(5, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Muy eficiente', 'Excelente', 'Excelente', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3),
(6, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Muy eficiente', 'Excelente', 'Excelente', 3, 3, 3, 3, 3, 3, 3, 3, 3, 3),
(7, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Muy eficiente', 'Excelente', 'Excelente', 1, 1, 5, 1, 5, 1, 1, 1, 1, 1),
(8, 'rafael.flores@vallarta.tecmm.edu.mx', 'Muy eficiente', 'Bueno', 'Bueno', 1, 2, 2, 2, 5, 5, 5, 5, 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblegresadosu`
--

CREATE TABLE `tblegresadosu` (
  `egrId` int(11) NOT NULL,
  `egrCorreo` varchar(45) NOT NULL,
  `egrNumProfesionistas` varchar(20) DEFAULT NULL,
  `egrNivelArq` varchar(30) DEFAULT NULL,
  `egrNivelTur` varchar(30) DEFAULT NULL,
  `egrNivelTic` varchar(30) DEFAULT NULL,
  `egrNivelEle` varchar(30) DEFAULT NULL,
  `egrNivelIge` varchar(30) DEFAULT NULL,
  `egrNivelSis` varchar(30) DEFAULT NULL,
  `egrNivelGas` varchar(30) DEFAULT NULL,
  `egrPerArq` varchar(200) DEFAULT NULL,
  `egrPerTur` varchar(200) DEFAULT NULL,
  `egrPerGas` varchar(200) DEFAULT NULL,
  `egrPerTic` varchar(200) DEFAULT NULL,
  `egrPerEle` varchar(200) DEFAULT NULL,
  `egrPerIge` varchar(200) DEFAULT NULL,
  `egrPerSis` varchar(200) DEFAULT NULL,
  `cgrNivelArq` varchar(60) DEFAULT NULL,
  `cgrNivelTur` varchar(60) DEFAULT NULL,
  `cgrNivelTic` varchar(60) DEFAULT NULL,
  `cgrNivelEle` varchar(60) DEFAULT NULL,
  `cgrNivelIge` varchar(60) DEFAULT NULL,
  `cgrNivelSis` varchar(60) DEFAULT NULL,
  `cgrNivelGas` varchar(60) DEFAULT NULL,
  `egrRequisitosEmpresa` varchar(3000) DEFAULT NULL,
  `egrCarrerasDemanda` varchar(3000) DEFAULT NULL,
  `egrEspNegocios` varchar(5) DEFAULT NULL,
  `egrEspMoviles` varchar(5) DEFAULT NULL,
  `egrEspSustentable` varchar(5) DEFAULT NULL,
  `egrEspRedes` varchar(5) DEFAULT NULL,
  `egrEspAutomatizacion` varchar(5) DEFAULT NULL,
  `egrEspCulinarias` varchar(5) DEFAULT NULL,
  `egrEspTurism` varchar(5) DEFAULT NULL,
  `egrEspecialidadSugerida` varchar(120) DEFAULT NULL,
  `egrLineaInnovacion` varchar(5) DEFAULT NULL,
  `egrLineaEstrategicas` varchar(5) DEFAULT NULL,
  `egrMaestriaSugerida` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblegresadosu`
--

INSERT INTO `tblegresadosu` (`egrId`, `egrCorreo`, `egrNumProfesionistas`, `egrNivelArq`, `egrNivelTur`, `egrNivelTic`, `egrNivelEle`, `egrNivelIge`, `egrNivelSis`, `egrNivelGas`, `egrPerArq`, `egrPerTur`, `egrPerGas`, `egrPerTic`, `egrPerEle`, `egrPerIge`, `egrPerSis`, `cgrNivelArq`, `cgrNivelTur`, `cgrNivelTic`, `cgrNivelEle`, `cgrNivelIge`, `cgrNivelSis`, `cgrNivelGas`, `egrRequisitosEmpresa`, `egrCarrerasDemanda`, `egrEspNegocios`, `egrEspMoviles`, `egrEspSustentable`, `egrEspRedes`, `egrEspAutomatizacion`, `egrEspCulinarias`, `egrEspTurism`, `egrEspecialidadSugerida`, `egrLineaInnovacion`, `egrLineaEstrategicas`, `egrMaestriaSugerida`) VALUES
(1, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', NULL, '', '', '', ''),
(2, 'rafa.flores@vallarta', 'on', 'on', 'on', '', 'on', 'on', 'on', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', NULL, 'fchtgchg', 'on', 'on', 'tyftyc'),
(3, 'luis.lopez@vallarta.tecmm.edu.mx', 'on', 'on', 'on', 'on', 'on', 'on', 'on', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', NULL, 'dsasads', 'on', 'on', ' wedcds'),
(4, 'luis.lopez@vallarta.tecmm.edu.mx', 'on', 'on', 'on', 'on', 'on', 'on', 'on', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'on', 'on', 'on', 'on', 'on', 'on', 'on', 'on', NULL, 'dsasads', 'on', 'on', ' wedcds'),
(5, 'lord_1@gmail.com', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', NULL, '', '', '', ''),
(6, 'lord_1@gmail.com', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '', '', NULL, '', '', '', ''),
(7, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', ''),
(8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', NULL, '', '', '', ''),
(9, '', '1', 'completamente', 'completamente', 'completamente', 'completamente', 'completamente', 'completamente', 'completamente', '1', '2', '3', '4', '5', '6', '7', '', '', '', '', '', '', '', 'Array', 'Array', '1', '1', '1', '1', '1', '1', '1', 'hectorjose', '1', '1', 'LordPalomino'),
(10, '', '1', 'medianamente', 'medianamente', 'ligeramente', 'ligeramente', 'completamente', 'completamente', 'medianamente', '1', '2', '3', '4', '5', '6', '7', '', '', '', '', '', '', '', 'Array', 'Array', '1', '1', '1', '1', '1', '1', '1', 'hectorjose', '1', '1', 'LordPalomino'),
(11, '', '1', 'medianamente', 'medianamente', 'ligeramente', 'ligeramente', 'completamente', 'completamente', 'ningunarelacion', '1', '2', '3', '4', '5', '6', '7', '', '', '', '', '', '', '', 'Array', 'Array', '1', '1', '1', '1', '1', '1', '1', 'hectorjose', '1', '1', 'LordPalomino'),
(12, '', '1', 'medianamente', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', '1', '2', '3', '4', '5', '6', '7', '', 'medianamente', 'ligeramente', 'ligeramente', 'completamente', 'completamente', 'ningunarelacion', 'Array', 'Array', '1', '1', '1', '1', '1', '1', '1', 'hectorjose', '1', '1', 'LordPalomino'),
(13, '', '1', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', '1', '2', '3', '4', '5', '6', '7', 'medianamente', 'medianamente', 'ligeramente', 'ligeramente', 'completamente', 'completamente', 'ningunarelacion', 'Array', 'Array', '1', '1', '1', '1', '1', '1', '1', 'hectorjose', '1', '1', 'LordPalomino'),
(14, '', '1', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', '1', '2', '3', '4', '5', '6', '7', 'medianamente', 'medianamente', 'ligeramente', 'ligeramente', 'completamente', 'completamente', 'ningunarelacion', 'Ãrea o campo de estudio-TitulaciÃ³n-Experiencia laboral/ prÃ¡ctica-Posicionamiento de la instituciÃ³n de egresos-Conocimientos de idiomas extranjeros-Recomendaciones/ Referencias-Personalidad/ actitudes-Capacidad de liderazgo-Otro-', 'Arquitectura-Turismo-GastronomÃ­a-IngenierÃ­a en tecnologÃ­as de la informaciÃ³n y comunicaciones-IngenierÃ­a en electromecÃ¡nica-IngenierÃ­a en gestiÃ³n empresarial-IngenierÃ­a en sistemas computacionales-Otro-', '1', '1', '1', '1', '1', '1', '1', 'hectorjose', '1', '1', 'LordPalomino'),
(15, 'lord_Palomino@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(16, 'lord_Palomino@gmail.com', '1', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', 'mandosuperior', '1', '1', '1', '1', '1', '1', '1', 'completamente', 'completamente', 'completamente', 'completamente', 'completamente', 'completamente', 'completamente', 'Ãrea o campo de estudio-', 'Arquitectura-', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblempresa`
--

CREATE TABLE `tblempresa` (
  `empId` int(11) NOT NULL,
  `empCorreo` varchar(70) NOT NULL,
  `empDatos` varchar(20) DEFAULT NULL,
  `empGiro` varchar(80) DEFAULT NULL,
  `empRazonS` varchar(100) DEFAULT NULL,
  `empCalle` varchar(80) DEFAULT NULL,
  `empNumero` varchar(10) DEFAULT NULL,
  `empColonia` varchar(80) DEFAULT NULL,
  `empCodigoP` varchar(8) DEFAULT NULL,
  `empCiudad` varchar(60) DEFAULT NULL,
  `empMunicipio` varchar(60) DEFAULT NULL,
  `empEstado` varchar(45) DEFAULT NULL,
  `empTelefono` varchar(20) DEFAULT NULL,
  `empExtension` varchar(20) DEFAULT NULL,
  `empPagina` varchar(60) DEFAULT NULL,
  `empJefe` varchar(60) DEFAULT NULL,
  `empSectorP` varchar(30) DEFAULT NULL,
  `empSectorS` varchar(30) DEFAULT NULL,
  `empSectorT` varchar(30) DEFAULT NULL,
  `empTamano` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblempresa`
--

INSERT INTO `tblempresa` (`empId`, `empCorreo`, `empDatos`, `empGiro`, `empRazonS`, `empCalle`, `empNumero`, `empColonia`, `empCodigoP`, `empCiudad`, `empMunicipio`, `empEstado`, `empTelefono`, `empExtension`, `empPagina`, `empJefe`, `empSectorP`, `empSectorS`, `empSectorT`, `empTamano`) VALUES
(9, 'jose.villalobos@vallarta.tecmm.edu.mx', 'publico', 'Educativo', 'ITJMMPV', 'Corea del sur', '600', 'El mangal', '48338', 'pv', 'Puerto Vallarta', 'Jalisco', '2265600', '2265600', 'www.tecvallarta.edu.mx', 'Jefe', 'Agroindustria', 'Industrial', 'Hotelero', 'MicroEmpresa (10-30)'),
(10, 'jose.villalobos@vallarta.tecmm.edu.mx', 'publico', 'Educativo', 'ITJMMPV', 'Corea del sur', '600', 'El mangal', '48338', 'pv', 'Puerto Vallarta', 'Jalisco', '2265600', '2265600', 'www.tecvallarta.edu.mx', 'Jefe', 'Agroindustria', 'Industrial', 'Hotelero', 'MicroEmpresa (10-30)'),
(11, 'jose.villalobos@vallarta.tecmm.edu.mx', 'publico', 'Educativo', 'ITJMMPV', 'Corea del sur', '600', 'El mangal', '48338', 'pv', 'Puerto Vallarta', 'Jalisco', '2265600', '2265600', 'www.tecvallarta.edu.mx', 'Jefe', 'Agroindustria', 'Industrial', 'Hotelero', 'MicroEmpresa (10-30)'),
(12, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13, 'jose.villalobos@vallarta.tecmm.edu.mx', 'publico', 'Educativo', 'ITJMMPV', 'C', '600', 'El mangal', '48338', 'pv', 'Puerto Vallarta', 'Jalisco', '2265600', '2265600', 'www.tecvallarta.edu.mx', 'Arce-Jefe de división', 'Agroindustria', 'Industrial', 'Hotelero', 'MicroEmpresa (10-30)'),
(14, 'rafael.flores@vallarta.tecmm.edu.mx', 'publico', 'Educativo', 'Walmart', 'Corea del sur', '342', 'El mangal', '48338', 'pv', 'Puerto Vallarta', 'Jalisco', '2265600', '3244656', 'www.tecvallarta.edu.mx', 'Jefe', 'Agroindustria', 'Industrial', 'Restaurante', 'MicroEmpresa (10-30)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblespectativas`
--

CREATE TABLE `tblespectativas` (
  `espId` int(11) NOT NULL,
  `espCorreo` varchar(60) DEFAULT NULL,
  `espCursos` varchar(5) DEFAULT NULL,
  `espCursoCual` varchar(100) DEFAULT NULL,
  `espPostgrado` varchar(5) DEFAULT NULL,
  `espCapacitarse` varchar(5) DEFAULT NULL,
  `espParticipacion` varchar(200) DEFAULT NULL,
  `espPOrgSociales` varchar(5) DEFAULT NULL,
  `espSocialesDesc` varchar(200) DEFAULT NULL,
  `espPOrgProfesionistas` varchar(5) DEFAULT NULL,
  `espProfesionistasDesc` varchar(200) DEFAULT NULL,
  `espPOrgEgresados` varchar(5) DEFAULT NULL,
  `espEgresadosDesc` varchar(200) DEFAULT NULL,
  `espComentarios` varchar(200) DEFAULT NULL,
  `espRecomendacion` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblespectativas`
--

INSERT INTO `tblespectativas` (`espId`, `espCorreo`, `espCursos`, `espCursoCual`, `espPostgrado`, `espCapacitarse`, `espParticipacion`, `espPOrgSociales`, `espSocialesDesc`, `espPOrgProfesionistas`, `espProfesionistasDesc`, `espPOrgEgresados`, `espEgresadosDesc`, `espComentarios`, `espRecomendacion`) VALUES
(8, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Si', 'Redes', 'Si', 'Si', 'Profesionistas', 'Si', 'Sociales', 'Si', '', 'Si', 'Egresados', 'Ok', 'OK'),
(9, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Si', 'Redes', 'Si', 'Si', 'Profesionistas', 'Si', 'Sociales', 'Si', 'Profesionistas', 'Si', 'Egresados', 'Ok', 'OK'),
(10, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Si', 'Redes', 'Si', 'Si', 'Profesionistas', 'Si', 'Sociales', 'Si', 'Profesionistas', 'Si', 'Egresados', 'Ok', 'OK'),
(11, '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Si', 'Mecatrónica', 'Si', 'Si', 'Redes', 'Si', 'Sociales de limpieza', 'Si', 'Profesores', 'Si', 'Egresados', 'Ok', 'Ok'),
(13, 'rafael.flores@vallarta.tecmm.edu.mx', 'Si', 'Sql', 'Si', 'Si', 'sociedad civil', 'Si', 'sociedad civil', 'Si', 'red de investigadores', 'No', '', 'Ok', 'Jalisco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblorganismo`
--

CREATE TABLE `tblorganismo` (
  `orgId` int(11) NOT NULL,
  `orgCorreo` varchar(70) NOT NULL,
  `orgNombre` varchar(120) NOT NULL,
  `orgDomicilio` varchar(80) DEFAULT NULL,
  `orgCiudad` varchar(80) DEFAULT NULL,
  `orgMunicipio` varchar(80) DEFAULT NULL,
  `orgEstado` varchar(45) DEFAULT NULL,
  `orgTelefono` varchar(20) DEFAULT NULL,
  `orgSector` varchar(10) DEFAULT NULL,
  `orgTamano` varchar(10) DEFAULT NULL,
  `orgActividad` varchar(45) DEFAULT NULL,
  `orgClasificacion` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblorganismo`
--

INSERT INTO `tblorganismo` (`orgId`, `orgCorreo`, `orgNombre`, `orgDomicilio`, `orgCiudad`, `orgMunicipio`, `orgEstado`, `orgTelefono`, `orgSector`, `orgTamano`, `orgActividad`, `orgClasificacion`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', ''),
(2, 'rafa.flores@vallarta', 'c', 'a', 's', 's', 'a', '123123', 'on', 'on', 'on', ''),
(3, 'luis.lopez@vallarta.tecmm.edu.mx', 'as', 'cads', 'dsd', 'dsds', 'cdsds', '322323', 'on', 'on', 'on', ' dadds'),
(4, 'luis.lopez@vallarta.tecmm.edu.mx', 'as', 'cads', 'dsd', 'dsds', 'cdsds', '322323', 'on', 'on', 'on', ' dadds'),
(5, 'lord_1@gmail.com', 'asdsadfasdasd', '', '', '', '', '', '', '', '', ''),
(6, 'lord_1@gmail.com', 'asdsadfasdasd', '', '', '', '', '', '', '', '', ''),
(7, 'lord_1@gmail.com', 'asdsadfasdasd', '', '', '', '', '', '', '', '', ''),
(8, 'lord_Palomino@gmail.com', '', '', '', '', '', '', '', '', '', ''),
(9, 'lord_Palomino@gmail.com', '1', '1', '1', '1', '1', '1', 'publica', 'micro', 'pya', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblperfil`
--

CREATE TABLE `tblperfil` (
  `perId` int(11) NOT NULL,
  `perCorreo` varchar(70) NOT NULL,
  `perNombreC` varchar(60) DEFAULT NULL,
  `perFecha` varchar(30) DEFAULT NULL,
  `perSexo` varchar(10) DEFAULT NULL,
  `perEstadoC` varchar(20) DEFAULT NULL,
  `perCalle` varchar(60) DEFAULT NULL,
  `perNumero` varchar(5) DEFAULT NULL,
  `perColonia` varchar(45) DEFAULT NULL,
  `perCPostal` varchar(5) DEFAULT NULL,
  `perCiudad` varchar(50) DEFAULT NULL,
  `perCelular` varchar(10) DEFAULT NULL,
  `perTelefono` varchar(20) DEFAULT NULL,
  `perEspecialidad` varchar(45) DEFAULT NULL,
  `perAnioEgreso` varchar(4) DEFAULT NULL,
  `perTitulado` varchar(10) DEFAULT NULL,
  `perIngles` float DEFAULT NULL,
  `perOtroI` float DEFAULT NULL,
  `perPaquetesComp` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblperfil`
--

INSERT INTO `tblperfil` (`perId`, `perCorreo`, `perNombreC`, `perFecha`, `perSexo`, `perEstadoC`, `perCalle`, `perNumero`, `perColonia`, `perCPostal`, `perCiudad`, `perCelular`, `perTelefono`, `perEspecialidad`, `perAnioEgreso`, `perTitulado`, `perIngles`, `perOtroI`, `perPaquetesComp`) VALUES
(24, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Jose Martin Villalobos Salmeron', '31-05-1982', 'Masculino', 'Soltero', 'Genaro padilla ', '757', 'Morelos y pavón', '48290', 'Puerto Vallarta', '3221181049', '3224545454', 'Redes de datos', '2005', 'Titulado', 100, 100, 'Microsoft Office'),
(25, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Jose Martin Villalobos Salmeron', '31-05-1982', 'Masculino', 'Soltero', 'Genaro padilla ', '757', 'Morelos y pavón', '48290', 'Puerto Vallarta', '3221181049', '3224545454', 'Redes de datos', '2005', 'Titulado', 100, 100, 'Microsoft Office'),
(26, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Jose Martin Villalobos Salmeron', '31-05-1982', 'Masculino', 'Soltero', 'Genaro padilla ', '757', 'Morelos y pavón', '48290', 'Puerto Vallarta', '3221181049', '3224545454', 'Redes de datos', '2005', 'Titulado', 100, 100, 'Microsoft Office'),
(27, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Jose Martin Villalobos Salmeron', '31-05-1982', 'Masculino', 'Casado', 'Genaro padilla ', '757', 'Morelos y pavón', '48290', 'Puerto Vallarta', '3221181049', '3224545454', 'Móviles', '2005', 'Titulado', 100, 0, 'Microsoft Office'),
(28, 'rafael.flores@vallarta.tecmm.edu.mx', 'Rafael Flores', '10-12-1980', 'Masculino', 'Casado', 'El pitillal', '454', 'El mangal', '48290', 'Puerto Vallarta', '3221181049', '3224545454', 'Moviles', '2005', 'Titulado', 50, 50, 'Office');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblrecursos`
--

CREATE TABLE `tblrecursos` (
  `recId` int(11) NOT NULL,
  `recCorreo` varchar(70) NOT NULL,
  `recCalidadDoc` int(11) DEFAULT NULL,
  `recPlanE` int(11) DEFAULT NULL,
  `recInvestigacion` int(11) DEFAULT NULL,
  `recEnfasisInv` int(11) DEFAULT NULL,
  `recSatisfaccion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblrecursos`
--

INSERT INTO `tblrecursos` (`recId`, `recCorreo`, `recCalidadDoc`, `recPlanE`, `recInvestigacion`, `recEnfasisInv`, `recSatisfaccion`) VALUES
(17, 'jose.villalobos@vallarta.tecmm.edu.mx', 5, 5, 5, 5, 5),
(18, 'jose.villalobos@vallarta.tecmm.edu.mx', 5, 5, 5, 5, 5),
(19, 'jose.villalobos@vallarta.tecmm.edu.mx', 5, 5, 5, 5, 5),
(20, 'jose.villalobos@vallarta.tecmm.edu.mx', 1, 1, 1, 1, 1),
(21, 'rafael.flores@vallarta.tecmm.edu.mx', 2, 2, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblubicacion`
--

CREATE TABLE `tblubicacion` (
  `ubiId` int(11) NOT NULL,
  `ubiCorreo` varchar(70) NOT NULL,
  `ubiActividad` varchar(30) DEFAULT NULL,
  `ubiEstudia` varchar(30) DEFAULT NULL,
  `ubiEspecialidad` varchar(80) DEFAULT NULL,
  `ubiTiempoT` varchar(45) DEFAULT NULL,
  `ubiMedioE` varchar(45) DEFAULT NULL,
  `ubiRequisitos` varchar(30) DEFAULT NULL,
  `ubiIdioma` varchar(30) DEFAULT NULL,
  `ubiPHablar` varchar(5) DEFAULT NULL,
  `ubiPEscribir` varchar(5) DEFAULT NULL,
  `ubiPLeer` varchar(5) DEFAULT NULL,
  `ubiPEscuchar` varchar(5) DEFAULT NULL,
  `ubiAntiguedad` varchar(20) DEFAULT NULL,
  `ubiIngreso` varchar(30) DEFAULT NULL,
  `ubiNivel` varchar(20) DEFAULT NULL,
  `ubiCondicion` varchar(20) DEFAULT NULL,
  `ubiRelacion` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblubicacion`
--

INSERT INTO `tblubicacion` (`ubiId`, `ubiCorreo`, `ubiActividad`, `ubiEstudia`, `ubiEspecialidad`, `ubiTiempoT`, `ubiMedioE`, `ubiRequisitos`, `ubiIdioma`, `ubiPHablar`, `ubiPEscribir`, `ubiPLeer`, `ubiPEscuchar`, `ubiAntiguedad`, `ubiIngreso`, `ubiNivel`, `ubiCondicion`, `ubiRelacion`) VALUES
(16, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Programación', 'Especialidad', 'Aplicaciones móviles', 'Antes de egresar', 'Contactos personales', 'Examen de seleccion', 'Frances', '60', '60', '60', '60', 'Un año', 'Entre 6000 y 11000', 'Jefe de área', 'Base', '100'),
(17, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Programación', 'Especialidad', 'Aplicaciones móviles', 'Antes de egresar', 'Contactos personales', 'Examen de seleccion', 'Frances', '60', '60', '60', '60', 'Un año', 'Entre 6000 y 11000', 'Jefe de área', 'Base', '100'),
(18, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Programación', 'Especialidad', 'Aplicaciones móviles', 'Antes de egresar', 'Contactos personales', 'Examen de seleccion', 'Frances', '60', '60', '60', '60', 'Un año', 'Entre 6000 y 11000', 'Jefe de área', 'Base', '100'),
(19, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(20, 'jose.villalobos@vallarta.tecmm.edu.mx', 'Programación', 'Especialidad', 'Móviles', 'Antes de egresar', 'Bolsa del trabajo del plantel', 'Competencias Laborales', 'Ingles', '20', '20', '20', '20', 'Menos de un año', 'Menos de 6000', 'Técnico', 'Base', '0'),
(21, 'rafael.flores@vallarta.tecmm.edu.mx', 'Docencia', 'Doctorado', 'Móviles', 'Antes de egresar', 'Bolsa del trabajo del plantel', 'Competencias Laborales', 'Ingles', '80', '80', '80', '80', 'Dos años', 'Entre 6000 y 11000', 'Directivo', 'Contrato', '20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuarios`
--

CREATE TABLE `tblusuarios` (
  `usuId` int(11) NOT NULL,
  `usuNomina` int(11) DEFAULT NULL,
  `usuNombreCompleto` varchar(60) DEFAULT NULL,
  `usuCorreo` varchar(60) DEFAULT NULL,
  `usuUsuario` varchar(10) DEFAULT NULL,
  `usuContraseña` varchar(8) DEFAULT NULL,
  `usuTipo` int(11) DEFAULT NULL,
  `usuUnidadA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblcompetencias`
--
ALTER TABLE `tblcompetencias`
  ADD PRIMARY KEY (`comId`);

--
-- Indices de la tabla `tbldesempeno`
--
ALTER TABLE `tbldesempeno`
  ADD PRIMARY KEY (`desId`);

--
-- Indices de la tabla `tblegresadosu`
--
ALTER TABLE `tblegresadosu`
  ADD PRIMARY KEY (`egrId`,`egrCorreo`);

--
-- Indices de la tabla `tblempresa`
--
ALTER TABLE `tblempresa`
  ADD PRIMARY KEY (`empId`);

--
-- Indices de la tabla `tblespectativas`
--
ALTER TABLE `tblespectativas`
  ADD PRIMARY KEY (`espId`);

--
-- Indices de la tabla `tblorganismo`
--
ALTER TABLE `tblorganismo`
  ADD PRIMARY KEY (`orgId`,`orgCorreo`);

--
-- Indices de la tabla `tblperfil`
--
ALTER TABLE `tblperfil`
  ADD PRIMARY KEY (`perId`,`perCorreo`);

--
-- Indices de la tabla `tblrecursos`
--
ALTER TABLE `tblrecursos`
  ADD PRIMARY KEY (`recId`);

--
-- Indices de la tabla `tblubicacion`
--
ALTER TABLE `tblubicacion`
  ADD PRIMARY KEY (`ubiId`);

--
-- Indices de la tabla `tblusuarios`
--
ALTER TABLE `tblusuarios`
  ADD PRIMARY KEY (`usuId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblcompetencias`
--
ALTER TABLE `tblcompetencias`
  MODIFY `comId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbldesempeno`
--
ALTER TABLE `tbldesempeno`
  MODIFY `desId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tblegresadosu`
--
ALTER TABLE `tblegresadosu`
  MODIFY `egrId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `tblempresa`
--
ALTER TABLE `tblempresa`
  MODIFY `empId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tblespectativas`
--
ALTER TABLE `tblespectativas`
  MODIFY `espId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `tblorganismo`
--
ALTER TABLE `tblorganismo`
  MODIFY `orgId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tblperfil`
--
ALTER TABLE `tblperfil`
  MODIFY `perId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `tblrecursos`
--
ALTER TABLE `tblrecursos`
  MODIFY `recId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `tblubicacion`
--
ALTER TABLE `tblubicacion`
  MODIFY `ubiId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
