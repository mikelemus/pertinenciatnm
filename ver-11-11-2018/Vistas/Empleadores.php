<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta charset="UTF-8">


	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="CSS/Footer.css">
    <link rel="stylesheet" href="CSS/style.css">

	<!-- <link rel="stylesheet" href="css/bootstrap-them.min.css">

    https://www.w3schools.com/tags/att_input_required.asp
    https://www.w3schools.com/html/html_form_input_types.asp
    https://www.w3schools.com/bootstrap/bootstrap_get_started.asp
    https://docs.google.com/forms/d/e/1FAIpQLScsNGOXW2c9SlNSjDNLnB6O-Mv-qWT-Lkq1wkU1KCfcMIVfZQ/viewform

	 -->
	<title>Programa de Pertinencia</title>
</head>
<body>
	<div class="container">
  	<header class="row">
			<div class="col-md-12">
      	<img src="imagen/HeaderCIITNE.jpg" class="img-responsive" alt="Imagen responsive" height="160" width="1140">
      </div>
		</header>
    <?php require 'nav.php';?>
    <div class="jumbotron">
      <div class="container text-danger text-center">
        <h2>CUESTIONARIO PARA EMPLEADORES </h2>
      </div>
      <div class="container text-center"> <h3>Sector Productivo y de Servicios</h3></div>
      <div class="container text-center">
        <p>INSTITUTO TECNOLÓGICO SUPERIOR JOSÉ MARIO MOLINA PASQUEL Y HENRÍQUEZ UNIDAD ACADÉMICA PUERTO VALLARTA, JALISCO</p>
      </div>
    </div>
            <form class="" action="../Controles/conInsertarEmpleador.php" method="POST">
      <div class="jumbotron">
        <div class="form-group">
          <label class="col-sm-12 text-center" ><p>Introduce tu email</p></label>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" placeholder="email@dominio.com" name="orgCorreo" required>
        </div>
      </div>
      <div class="jumbotron col-sm-12">
        <div class="container text-justify col-sm-8 col-sm-offset-2">
      <button type="button" class="btn btn-danger btn-lg btn-block alert"><H5><label class="glyphicon glyphicon-align-left  glyphicon-info-sign"></label> INSTRUCIONES</H5></button>

        </div>
      </div>

<div class="jumbotron col-sm-12">

  <h2>A. DATOS GENERALES DE LA EMPRESA U ORGANISMO</h2>

  <p>Empleador</p>



    <div class="form-group">
      <label class=" col-sm-10" for="Empresa_domicilo">1.Nombre Comercial de la Empresa:</label>
      <input type="text" class="form-control" id="Empresa_nombre" placeholder="Nombre de la  Empresa" name="orgNombre" required>
    </div>

    <div class="form-group">
      <label class=" col-sm-10" for="Empresa_domicilo">Domicilio:</label>
      <input type="text" class="form-control" id="Empresa_domicilo" placeholder="Direccion Completa" name="orgDomicilio" required>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_ciudad">Ciudad:</label>

        <input type="text" class="form-control" id="Empresa_ciudad" placeholder="Ciudad" name="orgCiudad" required>

    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_municipio">Municipio:</label>

        <input type="text" class="form-control" id="Empresa_municipio" placeholder="Municipio" name="orgMunicipio" required>

    </div>


     <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_estado">Estado:</label>

        <input type="rfc" class="form-control" id="Empresa_estado" placeholder="Estado" name="orgEstado" required>

    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_tel">Teléfono:</label>

        <input type="number" class="form-control" id="Empresa_tel" placeholder="Numero a 10 digitos" name="orgTelefono" required>

    </div>

    <!--<div class="form-group">
      <label class="control-label col-sm-2" for="Empresa_mail">Email de la Empresa:</label>
      <input type="email" class="form-control" id="Empresa_mail" placeholder="email@dominio.com" name="Empresa_mail" disabled="true">
    </div>
  -->


    <div class="form-group">
      <label class="control-label">2.Su empresa u organismo es:</label>
      <div class="radio">
      <label><input type="radio" name="orgSector" required>Pública</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgSector">Privada</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgSector">Social</label>
    </div>
</div>


<div class="form-group">
      <label class="control-label" for="Empresa_tipo">3.-Tamaño de la empresa u organismo:</label>
     <div class="radio">
      <label><input type="radio" name="orgTamano" required>Microempresa (1 -30)</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgTamano">Pequeña (31-100)</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgTamano" >Mediana (101-500)</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgTamano" >  Grande (más de 500)</label>
    </div>

  </div>

   <div class="form-group">
      <label class="control-label" for="Empresa_tipo">4.-Actividad de su empresa u organismo</label>
     <div class="radio">
      <label><input type="radio" name="orgActividad" required>Pesca y Acuicultura</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgActividad">Alimentos y Bebidas</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgActividad">Textiles, Vestido y Cuero</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgActividad">Madera y sus productos</label>
    </div>

     <div class="radio ">
      <label><input type="radio" name="orgActividad">Papel, Imprenta y editoriales</label>
    </div>

     <div class="radio ">
      <label><input type="radio" name="orgActividad">Química</label>
    </div>

    <div class="radio ">
      <label><input type="radio" name="orgActividad">Otros</label>
    </div>
  </div>

    <div class="form-group">
      <label class="control-label" for="Empresa_tipo">5.- Indique a cuál clasificación corresponde su empresa</label>
     <div class="radio">
      <label><input type="radio" name="orgClasificacion" required>Minerales no metálicos</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgClasificacion">Industrias metálicas básicas</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="orgClasificacion" >Productos metálicos, maquinaria y equipo</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" >Construcción</label>
    </div>
     <div class="radio ">
      <label><input type="radio" name="orgClasificacion" >Electricidad, gas y agua</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" >Comercial y turismo</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" >Transporte, almacenaje y comunicaciones/label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" >Servicios financieros, seguros, actividades inmobiliarias y de alquiler</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="orgClasificacion" >Educación</label>
    </div>
    <div class="input-group">
      <span class="input-group-addon">
        <input type="radio" name ="rb5" aria-label="...">
        Otro
      </span>
        <input type="text" class="form-control" aria-label="..." name="orgClasificacion">
    </div><!-- /input-group -->
  </div>
  <h2>B. UBICACIÓN LABORAL DE LOS EGRESADOS</h2>

    <div class="form-group">
      <label class="control-label">6.-número de profesionistas con nivel de licenciatura que laboran en la empresa u organismo</label>
      <div class="radio">
      <label><input type="radio" name="egrNumProfesionistas" required>1</label>
    </div>
    <div class="radio">
      <label><input type="radio" name="egrNumProfesionistas">de 2 a 5</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="egrNumProfesionistas">de 6 a 8</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="egrNumProfesionistas">de 9-a 10</label>
    </div>
    <div class="radio ">
      <label><input type="radio" name="egrNumProfesionistas">más de 10</label>
    </div>
</div>

<div class="form-group">
      <label class="control-label">7.- Indique el nivel jerárquico que ocupan los alumnos del Instituto Tecnológico es su organización<br></label>
<div class="table-responsive">
<setfield required>
<table class=" col-sm-12 table table-striped table-hover" id="tabla">
    <thead>
      <tr>
        <th></th>
        <th>Mando superior</th>
        <th>Mando intermedio</th>
        <th>Supervisor o equivalente</th>
        <th>Técnico o auxiliar</th>
        <th>Ninguno</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Lic. Arquitectura</td>
        <td><label ><input type="radio" name="egrNivelArq"required></label></td>
        <td><label ><input type="radio" name="egrNivelArq"></label></td>
        <td><label ><input type="radio" name="egrNivelArq"></label></td>
        <td><label ><input type="radio" name="egrNivelArq"></label></td>
        <td><label ><input type="radio" name="egrNivelArq"></label></td>
      </tr>
      <tr>
        <td>Lic. Turismo</td>
        <td><label><input type="radio" name="egrNivelTur"required></label></td>
        <td><label><input type="radio" name="egrNivelTur"></label></td>
        <td><label><input type="radio" name="egrNivelTur"></label></td>
        <td><label><input type="radio" name="egrNivelTur"></label></td>
        <td><label><input type="radio" name="egrNivelTur"></label></td>
      </tr>
      <tr>
        <td>Lic. Gastronomia</td>
        <td><label><input type="radio" name="egrNivelGas"required></label></td>
        <td><label><input type="radio" name="egrNivelGas"></label></td>
        <td><label><input type="radio" name="egrNivelGas"></label></td>
        <td><label><input type="radio" name="egrNivelGas"></label></td>
        <td><label><input type="radio" name="egrNivelGas"></label></td>
      </tr>
      <!--<tr>
        <td>Ing. TIC's</td>
         <td><label ><input type="radio" name="egrNivelTur"required></label></td>
         <td><label ><input type="radio" name="egrNivelTur"></label></td>
         <td><label ><input type="radio" name="egrNivelTur"></label></td>
         <td><label ><input type="radio" name="egrNivelTur"></label></td>
         <td><label ><input type="radio" name="egrNivelTur"></label></td>
      </tr>-->
      <tr>
       <td>Ing en Tecnologías de información</td>
       <td><label ><input type="radio" name="egrNivelTic"required></label></td>
       <td><label ><input type="radio" name="egrNivelTic"></label></td>
       <td><label ><input type="radio" name="egrNivelTic"></label></td>
       <td><label ><input type="radio" name="egrNivelTic"></label></td>
       <td><label ><input type="radio" name="egrNivelTic"></label></td>
      </tr>
      <tr>
       <td>Ing Electromecánica</td>
       <td><label ><input type="radio" name="egrNivelEle"required></label></td>
       <td><label ><input type="radio" name="egrNivelEle"></label></td>
       <td><label ><input type="radio" name="egrNivelEle"></label></td>
       <td><label ><input type="radio" name="egrNivelEle"></label></td>
       <td><label ><input type="radio" name="egrNivelEle"></label></td>
      </tr>
      <tr>
        <td>Ing. En Gestion Empresarial</td>
        <td><label><input type="radio" name="egrNivelIge"required></label></td>
        <td><label><input type="radio" name="egrNivelIge"></label></td>
        <td><label><input type="radio" name="egrNivelIge"></label></td>
        <td><label><input type="radio" name="egrNivelIge"></label></td>
        <td><label><input type="radio" name="egrNivelIge"></label></td>
      </tr>
      <tr>
        <td>Ing. Sis. Computacionales</td>
        <td><label><input type="radio" name="egrNivelSis" required></label></td>
        <td><label><input type="radio" name="egrNivelSis"></label></td>
        <td><label><input type="radio" name="egrNivelSis"></label></td>
        <td><label><input type="radio" name="egrNivelSis"></label></td>
        <td><label><input type="radio" name="egrNivelSis"></label></td>
      </tr>
    </tbody>
  </table>
</setfield>
</div>
</div>

<div class="form-group">
      <label class="control-label">7.- Indique el nivel jerárquico que ocupan los alumnos del Instituto Tecnológico es su organización<br></label>
<div class="table-responsive">
<setfield required>
<table class=" col-sm-12 table table-striped table-hover" id="tabla">
    <thead>
      <tr>
        <th></th>
        <th>Perfil De Egreso</th>
       </tr>
    </thead>
    <tbody>
      <tr>
        <td>Lic. Arquitectura</td>
        <td><label ><input type="text" name="egrPerArq"></label></td>
        </tr>
      <tr>
        <td>Lic. Turismo</td>
        <td><label><input type="text" name="egrPerTur"></label></td>
       
      </tr>
      <tr>
        <td>Lic. Gastronomia</td>
        <td><label><input type="text" name="egrPerGas"></label></td>
        
      </tr>
     
      <tr>
       <td>Ing en Tecnologías de información</td>
       <td><label ><input type="text" name="egrPerTic"></label></td>
       
      </tr>
      <tr>
       <td>Ing Electromecánica</td>
       <td><label ><input type="text" name="egrPerEle"></label></td>
       
      </tr>
      <tr>
        <td>Ing. En Gestion Empresarial</td>
        <td><label><input type="text" name="egrPerIge"></label></td>
      
      </tr>
      <tr>
        <td>Ing. Sis. Computacionales</td>
        <td><label><input type="text" name="egrPerSis"></label></td>
     
      </tr>
    </tbody>
  </table>
</setfield>
</div>
</div>




<div class="form-group">
      <label class="control-label">8.- Congruencia entre el perfil profesional que desarrollan los egresados del Instituto Tecnológico en su empresa u organización. Del total de sus egresados anote el porcentaje corresponde</label>
     <div class="table-responsive">
     <table id="tabla" class=" col-sm-12 table table-striped table-hover">
    <thead>
      <tr>
        <th></th>
        <th>Completamente</th>
        <th>Medianamente</th>
        <th>Ligeramente</th>
        <th>Ninguna Relacion</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Lic. Arquitectura</td>
        <td><label ><input type="radio" name="cgrNivelArq"required></label></td>
        <td><label ><input type="radio" name="cgrNivelArq"></label></td>
        <td><label ><input type="radio" name="cgrNivelArq"></label></td>
        <td><label ><input type="radio" name="cgrNivelArq"></label></td>
        <td><label ><input type="radio" name="cgrNivelArq"></label></td>
      </tr>
      <tr>
        <td>Lic. Turismo</td>
        <td><label><input type="radio" name="cgrNivelTur"required></label></td>
        <td><label><input type="radio" name="cgrNivelTur"></label></td>
        <td><label><input type="radio" name="cgrNivelTur"></label></td>
        <td><label><input type="radio" name="cgrNivelTur"></label></td>
        <td><label><input type="radio" name="cgrNivelTur"></label></td>
      </tr>
      <tr>
        <td>Lic. Gastronomia</td>
        <td><label><input type="radio" name="cgrNivelGas"required></label></td>
        <td><label><input type="radio" name="cgrNivelGas"></label></td>
        <td><label><input type="radio" name="cgrNivelGas"></label></td>
        <td><label><input type="radio" name="cgrNivelGas"></label></td>
        <td><label><input type="radio" name="cgrNivelGas"></label></td>
      </tr>
     <tr>
       <td>Ing en Tecnologías de información</td>
       <td><label ><input type="radio" name="cgrNivelTic"required></label></td>
       <td><label ><input type="radio" name="cgrNivelTic"></label></td>
       <td><label ><input type="radio" name="cgrNivelTic"></label></td>
       <td><label ><input type="radio" name="cgrNivelTic"></label></td>
       <td><label ><input type="radio" name="cgrNivelTic"></label></td>
      </tr>
      <tr>
       <td>Ing Electromecánica</td>
       <td><label ><input type="radio" name="cgrNivelEle"required></label></td>
       <td><label ><input type="radio" name="cgrNivelEle"></label></td>
       <td><label ><input type="radio" name="cgrNivelEle"></label></td>
       <td><label ><input type="radio" name="cgrNivelEle"></label></td>
       <td><label ><input type="radio" name="cgrNivelEle"></label></td>
      </tr>
      <tr>
        <td>Ing. En Gestion Empresarial</td>
        <td><label><input type="radio" name="cgrNivelIge"required></label></td>
        <td><label><input type="radio" name="cgrNivelIge"></label></td>
        <td><label><input type="radio" name="cgrNivelIge"></label></td>
        <td><label><input type="radio" name="cgrNivelIge"></label></td>
        <td><label><input type="radio" name="cgrNivelIge"></label></td>
      </tr>
      <tr>
        <td>Ing. Sis. Computacionales</td>
        <td><label><input type="radio" name="cgrNivelSis" required></label></td>
        <td><label><input type="radio" name="cgrNivelSis"></label></td>
        <td><label><input type="radio" name="cgrNivelSis"></label></td>
        <td><label><input type="radio" name="cgrNivelSis"></label></td>
        <td><label><input type="radio" name="cgrNivelSis"></label></td>
      </tr>
    
    </tbody>
  </table>
</div>
</div>
<div class="form-group">
      <label class="control-label">9.- Requisitos que establece su empresa u organización para la contratación de personal con nivel licenciatura</label>
      <div class="radio">
      <label><input type="checkbox" name="egrRequisitosEmpresaAce" required>Área o campo de estudio</label>
    </div>
    <div class="radio">
      <label><input type="checkbox" name="egrRequisitosEmpresaTit">Titulación</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaExp">Experiencia laboral/ práctica( antes de egresar)</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaPos">Posicionamiento de la institución de egresos</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaCon">Conocimientos de idiomas extranjeros</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaRec">Recomendaciones/ Referencias</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaPer">Personalidad/ actitudes</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaCap">Capacidad de liderazgo</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrRequisitosEmpresaOtr">Otro</label>
    </div>
</div>
<!-- pregunta 9 -->


 <!-- pregunta 10 -->
<div class="form-group">
      <label class=" control-label">10. Carreras que demanda preferentemente su Empresa u organismo:</label>
     <div class="radio">
      <label><input type="checkbox" name="egrCarrerasDemandaArq">Arquitectura</label>
    </div>
    <div class="radio">
      <label><input type="checkbox" name="egrCarrerasDemandaTur">Turismo</label>
    </div>
    <div class="radio">
      <label><input type="checkbox" name="egrCarrerasDemandaGas" >Gastronomía</label>
    </div>
    <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemandaTic" > Ingeniería en tecnologías de la información y comunicaciones</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemandaEle" >Ingeniería en electromecánica</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemandaIge" >Ingeniería en gestión empresarial</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemandaSis" >Ingeniería en sistemas computacionales</label>
    </div>
        <div class="radio ">
      <label><input type="checkbox" name="egrCarrerasDemandaOtr" >Otro: </label>
    </div>
  </div>

     <!-- pregunta 11 -->
<div class="form-group">
      <label class="control-label"><h4>11. ¿Cómo considera las especialidades de las carreras que se ofertan en el Instituto Tecnológico Superior?</h4></label>

    <label class="control-label">Desarrollo de negocios e innovación tecnológica (Ing. En gestion empresarial):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspNegocios" required></label></td>
        <td><label ><input type="radio" name="egrEspNegocios"></label></td>
        <td><label ><input type="radio" name="egrEspNegocios"></label></td>
        <td><label ><input type="radio" name="egrEspNegocios"></label></td>
        <td><label ><input type="radio" name="egrEspNegocios"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Aplicaciones para dispositivos móviles (Ing. En sistemas computacionales):</label>
    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspMoviles" required></label></td>
        <td><label ><input type="radio" name="egrEspMoviles"></label></td>
        <td><label ><input type="radio" name="egrEspMoviles"></label></td>
        <td><label ><input type="radio" name="egrEspMoviles"></label></td>
        <td><label ><input type="radio" name="egrEspMoviles"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Desarrollo sustentable y urbanismo (Arquitectura):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspSustentable" required></label></td>
        <td><label ><input type="radio" name="egrEspSustentable"></label></td>
        <td><label ><input type="radio" name="egrEspSustentable"></label></td>
        <td><label ><input type="radio" name="egrEspSustentable"></label></td>
        <td><label ><input type="radio" name="egrEspSustentable"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Redes y comunicación de datos (Ing. En tecnologías de la información y la comunicación):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspRedes" required></label></td>
        <td><label ><input type="radio" name="egrEspRedes"></label></td>
        <td><label ><input type="radio" name="egrEspRedes"></label></td>
        <td><label ><input type="radio" name="egrEspRedes"></label></td>
        <td><label ><input type="radio" name="egrEspRedes"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Automatización y electrificación (Ing. Electromecánica):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspAutomatizacion" required></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion"></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion"></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion"></label></td>
        <td><label ><input type="radio" name="egrEspAutomatizacion"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Administración de tendencias culinarias de vanguardia (Lic. Gastronomía):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspCulinarias" required></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias"></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias"></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias"></label></td>
        <td><label ><input type="radio" name="egrEspCulinarias"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

<label class="col-sm-12">(Turismo):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrEspTurismo" required></label></td>
        <td><label ><input type="radio" name="egrEspTurismo"></label></td>
        <td><label ><input type="radio" name="egrEspTurismo"></label></td>
        <td><label ><input type="radio" name="egrEspTurismo"></label></td>
        <td><label ><input type="radio" name="egrEspTurismo"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

    </div>

     <!-- pregunta 12 -->
<div class="form-group">
    <label class="control-label" for="respuesta12">12. ¿Qué especialidad sugeriría usted? </label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="egrEspecialidadSugerida" required>

  </div>

 <!-- pregunta 13 -->
<div class="form-group">
      <label class="control-label"><h4>13. ¿Cómo considera las líneas de trabajo de la maestría en Administración que se oferta en el Tec Vallarta?</h4></label>

    <label class="col-sm-12">Innovación, Productividad y Tecnología Para la Competitividad Internacional (En administración):</label>

        <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrLineaInnovacion" required></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion"></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion"></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion"></label></td>
        <td><label ><input type="radio" name="egrLineaInnovacion"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

    <label class="col-sm-12">Estrategias para la alta dirección (En administración):</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Mala</td>
        <td><label ><input type="radio" name="egrLineaEstrategicas" required></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas"></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas"></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas"></label></td>
        <td><label ><input type="radio" name="egrLineaEstrategicas"></label></td>
        <td>Excelente</td>
      </tr>
    </tbody>
  </table>
</setfield>

    </div>

 <!-- pregunta 14 -->
<div class="form-group">
        <label class="control-label" for="respuesta12">14. ¿Qué maestria sugeriría usted?
</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="egrMaestriaSugerida" required>

  </div>

 <!-- pregunta 15 -->
<div class="form-group">
      <label class="control-label"><h2>C. COMPETENCIAS LABORALES</h2></label>

    <label class="control-label">15. En su opinión ¿qué competencias considera deben desarrollar los egresados del Tec Vallarta, para desempeñarse eficientemente en sus actividades laborales? Por favor evalúe conforme a las siguientes opciones:</label>

        <label class="col-sm-12">Habilidad para resolver conflictos:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comConflictos" required></label></td>
        <td><label ><input type="radio" name="comConflictos"></label></td>
        <td><label ><input type="radio" name="comConflictos"></label></td>
        <td><label ><input type="radio" name="comConflictos"></label></td>
        <td><label ><input type="radio" name="comConflictos"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Ortografía y redacción de documentos:</label>

    <setfield required>
<table class="table table-responsive table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comRedaccion" required></label></td>
        <td><label ><input type="radio" name="comRedaccion"></label></td>
        <td><label ><input type="radio" name="comRedaccion"></label></td>
        <td><label ><input type="radio" name="comRedaccion"></label></td>
        <td><label ><input type="radio" name="comRedaccion"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Mejora de procesos:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comProcesos" required></label></td>
        <td><label ><input type="radio" name="comProcesos"></label></td>
        <td><label ><input type="radio" name="comProcesos"></label></td>
        <td><label ><input type="radio" name="comProcesos"></label></td>
        <td><label ><input type="radio" name="comProcesos"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Trabajo en equipo:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comTrabajoE" required></label></td>
        <td><label ><input type="radio" name="comTrabajoE"></label></td>
        <td><label ><input type="radio" name="comTrabajoE"></label></td>
        <td><label ><input type="radio" name="comTrabajoE"></label></td>
        <td><label ><input type="radio" name="comTrabajoE"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Seguridad personal:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comSeguridad" required></label></td>
        <td><label ><input type="radio" name="comSeguridad"></label></td>
        <td><label ><input type="radio" name="comSeguridad"></label></td>
        <td><label ><input type="radio" name="comSeguridad"></label></td>
        <td><label ><input type="radio" name="comSeguridad"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Facilidad de palabra:</label>

        <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comFacilidad" required></label></td>
        <td><label ><input type="radio" name="comFacilidad"></label></td>
        <td><label ><input type="radio" name="comFacilidad"></label></td>
        <td><label ><input type="radio" name="comFacilidad"></label></td>
        <td><label ><input type="radio" name="comFacilidad"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Gestión de Proyectos:</label>

        <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comGestion" required></label></td>
        <td><label ><input type="radio" name="comGestion"></label></td>
        <td><label ><input type="radio" name="comGestion"></label></td>
        <td><label ><input type="radio" name="comGestion"></label></td>
        <td><label ><input type="radio" name="comGestion"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Puntualidad y Asistencia:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comPuntualidad" required></label></td>
        <td><label ><input type="radio" name="comPuntualidad"></label></td>
        <td><label ><input type="radio" name="comPuntualidad"></label></td>
        <td><label ><input type="radio" name="comPuntualidad"></label></td>
        <td><label ><input type="radio" name="comPuntualidad"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Cumplimiento de las normas:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comCumplimiento" required></label></td>
        <td><label ><input type="radio" name="comCumplimiento"></label></td>
        <td><label ><input type="radio" name="comCumplimiento"></label></td>
        <td><label ><input type="radio" name="comCumplimiento"></label></td>
        <td><label ><input type="radio" name="comCumplimiento"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Integración al trabajo:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comIntegracion" required></label></td>
        <td><label ><input type="radio" name="comIntegracion"></label></td>
        <td><label ><input type="radio" name="comIntegracion"></label></td>
        <td><label ><input type="radio" name="comIntegracion"></label></td>
        <td><label ><input type="radio" name="comIntegracion"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Creatividad e innovación:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comCreatividad" required></label></td>
        <td><label ><input type="radio" name="comCreatividad"></label></td>
        <td><label ><input type="radio" name="comCreatividad"></label></td>
        <td><label ><input type="radio" name="comCreatividad"></label></td>
        <td><label ><input type="radio" name="comCreatividad"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Capacidad de negociación:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comNegociacion" required></label></td>
        <td><label ><input type="radio" name="comNegociacion"></label></td>
        <td><label ><input type="radio" name="comNegociacion"></label></td>
        <td><label ><input type="radio" name="comNegociacion"></label></td>
        <td><label ><input type="radio" name="comNegociacion"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Capacidad de abstracción, análisis y síntesis:</label>
    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comAbstraccion" required></label></td>
        <td><label ><input type="radio" name="comAbstraccion"></label></td>
        <td><label ><input type="radio" name="comAbstraccion"></label></td>
        <td><label ><input type="radio" name="comAbstraccion"></label></td>
        <td><label ><input type="radio" name="comAbstraccion"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Liderazgo y toma de decisiones:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comLiderazgo" required></label></td>
        <td><label ><input type="radio" name="comLiderazgo"></label></td>
        <td><label ><input type="radio" name="comLiderazgo"></label></td>
        <td><label ><input type="radio" name="comLiderazgo"></label></td>
        <td><label ><input type="radio" name="comLiderazgo"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Adaptación al cambio:</label>

    <setfield required>
<table class="table table-striped">
    <thead>
      <tr>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Menor</td>
        <td><label ><input type="radio" name="comAdaptacion" required></label></td>
        <td><label ><input type="radio" name="comAdaptacion"></label></td>
        <td><label ><input type="radio" name="comAdaptacion"></label></td>
        <td><label ><input type="radio" name="comAdaptacion"></label></td>
        <td><label ><input type="radio" name="comAdaptacion"></label></td>
        <td>Mayor</td>
      </tr>
    </tbody>
  </table>
</setfield>

        <label class="col-sm-12">Otras (especifique):</label>

    </div>

<!-- pregunta 16 -->
<!--<div class="form-group">
        <label class="control-label">16. Con base al desempeño laboral así como a las actividades laborales que realiza el egresado ¿Cómo considera su desempeño laboral respecto a su formación académica? Del total de egresados anote el porcentaje que corresponda :</label>
    <div class="table-responsive">
    <setfield required>
<table id="tabla" class="table table-striped ">
    <thead>
      <tr>
        <th></th>
        <th>20%</th>
        <th>40%</th>
        <th>60%</th>
        <th>80%</th>
        <th>100%</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Excelente</td>
        <td><label ><input type="radio" name="comDesempenoE" required></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
        <td><label ><input type="radio" name="comDesempenoE"></label></td>
      </tr>

      <tr>
        <td>Muy Bueno</td>
        <td><label ><input type="radio" name="comDesempenoMB" required></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
        <td><label ><input type="radio" name="comDesempenoMB"></label></td>
      </tr>

      <tr>
        <td>Bueno</td>
        <td><label ><input type="radio" name="comDesempenoB" required></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
        <td><label ><input type="radio" name="comDesempenoB"></label></td>
      </tr>

      <tr>
        <td>Regular</td>
        <td><label ><input type="radio" name="comDesempenoR" required></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
        <td><label ><input type="radio" name="comDesempenoR"></label></td>
      </tr>

      <tr>
        <td>Malo</td>
        <td><label ><input type="radio" name="comDesempenoM" required></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
        <td><label ><input type="radio" name="comDesempenoM"></label></td>
      </tr>
    </tbody>
  </table>
</setfield>
    </div>
    </div>

  -->

<!-- pregunta 17 -->
<div class="form-group">
    <label class="control-label" for="respuesta17">17. De acuerdo con las necesidades de su empresa u organismo, ¿qué sugiere para mejorar la formación de los egresados Tec Vallarta?</label>
    <input type="text" class="form-control" placeholder="Escriba su respuesta" name="comMejoraEgresado" required>


<div class="form-group">
      <label class="control-label col-sm-2" for="nombre">Comentarios:</label>
        <textarea class="form-control" id="det" placeholder="Quejas o comentarios" name="comComentario"></textarea>
 </div>


  <div class="form-group">
   <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
  </div>
</div>
</form>
</div>
<footer class="row">
			<div class="col-md-12">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</div>
		</footer>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
 <!-- bootbox code -->
    <script src="js/bootbox.min.js"></script>
    <script>
        $(document).on("click", ".alert", function(e) {
          bootbox.alert({
    message:"Por favor lea cuidadosamente y conteste este cuestionario de la siguiente manera, según sea el caso:<br><br>1. En el caso de preguntas cerradas, marque la que considere aprrb4iada de esta manera:(x) <br><br>2. En las preguntas de valoración se utiliza la escala del 1 al 5 en la que 1 es “muy malo” y 5 es “muy bueno”.<br><br>3. En los casos de preguntas abiertas dejamos un espacio para que usted escriba con mayúscula una respuesta (______________________________).<br>Si el espacio para su respuesta no es suficiente, por favor añada una hoja adicional al cuestionario.<br><br>4. Al final anexamos un inciso para comentarios y sugerencias, le agradeceremos anote ahí lo que considere prudente para mejorar nuestro sistema educativo o bien los temas que, a su juicio, omitimos en el cuestionario.<br><br>Gracias por su gentil colaboración.",
    backdrop: true,
    size: 'large'
});
        });
    </script>
</body>
</html>
