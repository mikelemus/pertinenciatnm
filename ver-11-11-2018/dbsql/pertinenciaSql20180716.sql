CREATE DATABASE  IF NOT EXISTS `bdpertinencia` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bdpertinencia`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: bdpertinencia
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbldesempeno`
--

DROP TABLE IF EXISTS `tbldesempeno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldesempeno` (
  `desId` int(11) NOT NULL AUTO_INCREMENT,
  `desCorreo` varchar(60) DEFAULT NULL,
  `desEficiencia` varchar(20) DEFAULT NULL,
  `desCalifica` varchar(20) DEFAULT NULL,
  `desUtilidad` varchar(20) DEFAULT NULL,
  `desAspecto1` int(11) DEFAULT NULL,
  `desAspecto2` int(11) DEFAULT NULL,
  `desAspecto3` int(11) DEFAULT NULL,
  `desAspecto4` int(11) DEFAULT NULL,
  `desAspecto5` int(11) DEFAULT NULL,
  `desAspecto6` int(11) DEFAULT NULL,
  `desAspecto7` int(11) DEFAULT NULL,
  `desAspecto8` int(11) DEFAULT NULL,
  `desAspecto9` int(11) DEFAULT NULL,
  `desAspecto10` int(11) DEFAULT NULL,
  PRIMARY KEY (`desId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldesempeno`
--

LOCK TABLES `tbldesempeno` WRITE;
/*!40000 ALTER TABLE `tbldesempeno` DISABLE KEYS */;
INSERT INTO `tbldesempeno` VALUES (4,'jose.villalobos@vallarta.tecmm.edu.mx','Muy eficiente','Excelente','Excelente',3,3,3,3,3,3,3,3,3,3),(5,'jose.villalobos@vallarta.tecmm.edu.mx','Muy eficiente','Excelente','Excelente',3,3,3,3,3,3,3,3,3,3),(6,'jose.villalobos@vallarta.tecmm.edu.mx','Muy eficiente','Excelente','Excelente',3,3,3,3,3,3,3,3,3,3);
/*!40000 ALTER TABLE `tbldesempeno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblempresa`
--

DROP TABLE IF EXISTS `tblempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblempresa` (
  `empId` int(11) NOT NULL AUTO_INCREMENT,
  `empCorreo` varchar(70) NOT NULL,
  `empDatos` varchar(20) DEFAULT NULL,
  `empGiro` varchar(80) DEFAULT NULL,
  `empRazonS` varchar(100) DEFAULT NULL,
  `empCalle` varchar(80) DEFAULT NULL,
  `empNumero` varchar(10) DEFAULT NULL,
  `empColonia` varchar(80) DEFAULT NULL,
  `empCodigoP` varchar(8) DEFAULT NULL,
  `empCiudad` varchar(60) DEFAULT NULL,
  `empMunicipio` varchar(60) DEFAULT NULL,
  `empEstado` varchar(45) DEFAULT NULL,
  `empTelefono` varchar(20) DEFAULT NULL,
  `empExtension` varchar(20) DEFAULT NULL,
  `empPagina` varchar(60) DEFAULT NULL,
  `empJefe` varchar(60) DEFAULT NULL,
  `empSectorP` varchar(30) DEFAULT NULL,
  `empSectorS` varchar(30) DEFAULT NULL,
  `empSectorT` varchar(30) DEFAULT NULL,
  `empTamano` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`empId`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblempresa`
--

LOCK TABLES `tblempresa` WRITE;
/*!40000 ALTER TABLE `tblempresa` DISABLE KEYS */;
INSERT INTO `tblempresa` VALUES (9,'jose.villalobos@vallarta.tecmm.edu.mx','publico','Educativo','ITJMMPV','Corea del sur','600','El mangal','48338','pv','Puerto Vallarta','Jalisco','2265600','2265600','www.tecvallarta.edu.mx','Jefe','Agroindustria','Industrial','Hotelero','MicroEmpresa (10-30)'),(10,'jose.villalobos@vallarta.tecmm.edu.mx','publico','Educativo','ITJMMPV','Corea del sur','600','El mangal','48338','pv','Puerto Vallarta','Jalisco','2265600','2265600','www.tecvallarta.edu.mx','Jefe','Agroindustria','Industrial','Hotelero','MicroEmpresa (10-30)'),(11,'jose.villalobos@vallarta.tecmm.edu.mx','publico','Educativo','ITJMMPV','Corea del sur','600','El mangal','48338','pv','Puerto Vallarta','Jalisco','2265600','2265600','www.tecvallarta.edu.mx','Jefe','Agroindustria','Industrial','Hotelero','MicroEmpresa (10-30)'),(12,'','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `tblempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblespectativas`
--

DROP TABLE IF EXISTS `tblespectativas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblespectativas` (
  `espId` int(11) NOT NULL AUTO_INCREMENT,
  `espCorreo` varchar(60) DEFAULT NULL,
  `espCursos` varchar(5) DEFAULT NULL,
  `espCursoCual` varchar(100) DEFAULT NULL,
  `espPostgrado` varchar(5) DEFAULT NULL,
  `espCapacitarse` varchar(5) DEFAULT NULL,
  `espParticipacion` varchar(200) DEFAULT NULL,
  `espPOrgSociales` varchar(5) DEFAULT NULL,
  `espSocialesDesc` varchar(200) DEFAULT NULL,
  `espPOrgProfesionistas` varchar(5) DEFAULT NULL,
  `espProfesionistasDesc` varchar(200) DEFAULT NULL,
  `espPOrgEgresados` varchar(5) DEFAULT NULL,
  `espEgresadosDesc` varchar(200) DEFAULT NULL,
  `espComentarios` varchar(200) DEFAULT NULL,
  `espRecomendacion` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`espId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblespectativas`
--

LOCK TABLES `tblespectativas` WRITE;
/*!40000 ALTER TABLE `tblespectativas` DISABLE KEYS */;
INSERT INTO `tblespectativas` VALUES (8,'jose.villalobos@vallarta.tecmm.edu.mx','Si','Redes','Si','Si','Profesionistas','Si','Sociales','Si','','Si','Egresados','Ok','OK'),(9,'jose.villalobos@vallarta.tecmm.edu.mx','Si','Redes','Si','Si','Profesionistas','Si','Sociales','Si','Profesionistas','Si','Egresados','Ok','OK'),(10,'jose.villalobos@vallarta.tecmm.edu.mx','Si','Redes','Si','Si','Profesionistas','Si','Sociales','Si','Profesionistas','Si','Egresados','Ok','OK'),(11,'','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `tblespectativas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblperfil`
--

DROP TABLE IF EXISTS `tblperfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblperfil` (
  `perId` int(11) NOT NULL AUTO_INCREMENT,
  `perCorreo` varchar(70) NOT NULL,
  `perNombreC` varchar(60) DEFAULT NULL,
  `perFecha` varchar(30) DEFAULT NULL,
  `perSexo` varchar(10) DEFAULT NULL,
  `perEstadoC` varchar(20) DEFAULT NULL,
  `perCalle` varchar(60) DEFAULT NULL,
  `perNumero` varchar(5) DEFAULT NULL,
  `perColonia` varchar(45) DEFAULT NULL,
  `perCPostal` varchar(5) DEFAULT NULL,
  `perCiudad` varchar(50) DEFAULT NULL,
  `perCelular` varchar(10) DEFAULT NULL,
  `perTelefono` varchar(20) DEFAULT NULL,
  `perEspecialidad` varchar(45) DEFAULT NULL,
  `perAnioEgreso` varchar(4) DEFAULT NULL,
  `perTitulado` varchar(10) DEFAULT NULL,
  `perIngles` float DEFAULT NULL,
  `perOtroI` float DEFAULT NULL,
  `perPaquetesComp` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`perId`,`perCorreo`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblperfil`
--

LOCK TABLES `tblperfil` WRITE;
/*!40000 ALTER TABLE `tblperfil` DISABLE KEYS */;
INSERT INTO `tblperfil` VALUES (24,'jose.villalobos@vallarta.tecmm.edu.mx','Jose Martin Villalobos Salmeron','31-05-1982','Masculino','Soltero','Genaro padilla ','757','Morelos y pavón','48290','Puerto Vallarta','3221181049','3224545454','Redes de datos','2005','Titulado',100,100,'Microsoft Office'),(25,'jose.villalobos@vallarta.tecmm.edu.mx','Jose Martin Villalobos Salmeron','31-05-1982','Masculino','Soltero','Genaro padilla ','757','Morelos y pavón','48290','Puerto Vallarta','3221181049','3224545454','Redes de datos','2005','Titulado',100,100,'Microsoft Office'),(26,'jose.villalobos@vallarta.tecmm.edu.mx','Jose Martin Villalobos Salmeron','31-05-1982','Masculino','Soltero','Genaro padilla ','757','Morelos y pavón','48290','Puerto Vallarta','3221181049','3224545454','Redes de datos','2005','Titulado',100,100,'Microsoft Office');
/*!40000 ALTER TABLE `tblperfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblrecursos`
--

DROP TABLE IF EXISTS `tblrecursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblrecursos` (
  `recId` int(11) NOT NULL AUTO_INCREMENT,
  `recCorreo` varchar(70) NOT NULL,
  `recCalidadDoc` int(11) DEFAULT NULL,
  `recPlanE` int(11) DEFAULT NULL,
  `recInvestigacion` int(11) DEFAULT NULL,
  `recEnfasisInv` int(11) DEFAULT NULL,
  `recSatisfaccion` int(11) DEFAULT NULL,
  PRIMARY KEY (`recId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblrecursos`
--

LOCK TABLES `tblrecursos` WRITE;
/*!40000 ALTER TABLE `tblrecursos` DISABLE KEYS */;
INSERT INTO `tblrecursos` VALUES (17,'jose.villalobos@vallarta.tecmm.edu.mx',5,5,5,5,5),(18,'jose.villalobos@vallarta.tecmm.edu.mx',5,5,5,5,5),(19,'jose.villalobos@vallarta.tecmm.edu.mx',5,5,5,5,5);
/*!40000 ALTER TABLE `tblrecursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblubicacion`
--

DROP TABLE IF EXISTS `tblubicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblubicacion` (
  `ubiId` int(11) NOT NULL AUTO_INCREMENT,
  `ubiCorreo` varchar(70) NOT NULL,
  `ubiActividad` varchar(30) DEFAULT NULL,
  `ubiEstudia` varchar(30) DEFAULT NULL,
  `ubiEspecialidad` varchar(80) DEFAULT NULL,
  `ubiTiempoT` varchar(45) DEFAULT NULL,
  `ubiMedioE` varchar(45) DEFAULT NULL,
  `ubiRequisitos` varchar(30) DEFAULT NULL,
  `ubiIdioma` varchar(30) DEFAULT NULL,
  `ubiPHablar` varchar(5) DEFAULT NULL,
  `ubiPEscribir` varchar(5) DEFAULT NULL,
  `ubiPLeer` varchar(5) DEFAULT NULL,
  `ubiPEscuchar` varchar(5) DEFAULT NULL,
  `ubiAntiguedad` varchar(20) DEFAULT NULL,
  `ubiIngreso` varchar(30) DEFAULT NULL,
  `ubiNivel` varchar(20) DEFAULT NULL,
  `ubiCondicion` varchar(20) DEFAULT NULL,
  `ubiRelacion` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ubiId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblubicacion`
--

LOCK TABLES `tblubicacion` WRITE;
/*!40000 ALTER TABLE `tblubicacion` DISABLE KEYS */;
INSERT INTO `tblubicacion` VALUES (16,'jose.villalobos@vallarta.tecmm.edu.mx','Programación','Especialidad','Aplicaciones móviles','Antes de egresar','Contactos personales','Examen de seleccion','Frances','60','60','60','60','Un año','Entre 6000 y 11000','Jefe de área','Base','100'),(17,'jose.villalobos@vallarta.tecmm.edu.mx','Programación','Especialidad','Aplicaciones móviles','Antes de egresar','Contactos personales','Examen de seleccion','Frances','60','60','60','60','Un año','Entre 6000 y 11000','Jefe de área','Base','100'),(18,'jose.villalobos@vallarta.tecmm.edu.mx','Programación','Especialidad','Aplicaciones móviles','Antes de egresar','Contactos personales','Examen de seleccion','Frances','60','60','60','60','Un año','Entre 6000 y 11000','Jefe de área','Base','100'),(19,'','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `tblubicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusuarios`
--

DROP TABLE IF EXISTS `tblusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusuarios` (
  `usuId` int(11) NOT NULL,
  `usuNomina` int(11) DEFAULT NULL,
  `usuNombreCompleto` varchar(60) DEFAULT NULL,
  `usuCorreo` varchar(60) DEFAULT NULL,
  `usuUsuario` varchar(10) DEFAULT NULL,
  `usuContraseña` varchar(8) DEFAULT NULL,
  `usuTipo` int(11) DEFAULT NULL,
  `usuUnidadA` int(11) DEFAULT NULL,
  PRIMARY KEY (`usuId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusuarios`
--

LOCK TABLES `tblusuarios` WRITE;
/*!40000 ALTER TABLE `tblusuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblusuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-16 19:53:35
